# -*- coding: utf-8 -*- 

import socket
import sys
import json

NCDB_IP = '192.168.50.173'
NCDB_PORT=59009
#NCDB_IP = '127.0.0.1'
#NCDB_PORT=2500

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((NCDB_IP, NCDB_PORT))


objectJson = '{"a":1, "b":1.5 , "c":["normal string", 1, 1.5]}'

_setup='{ \
    "setup": { \
    "message": "환영 Welcome to \\\"SeeEyes\\\"",\
    "font": "danganri",\
    "size": "12",\
    "color": "-16171111",\
    "speed": "7",\
    "from": "right",\
    "blink": "true",\
    "repeat": "0",\
    "sound": "bgm.mp3",\
    "mirror": "false",\
    "tts": "false" ,\
    "location" : "정문복도" ,\
    "normal_sound" : "normal.mp3",\
    "em_sound" : "em.mp3" ,\
    "fire_sound" : "fire.mp3" \
    } \
  }'
_call='{ \
    "call" : {\
        "type":"normal",\
        "model" : "nctb", \
        "ward":"1",\
        "room":"2",\
        "bed":"3"\
        } \
    }'

def jsetup():
    setup=json.loads(_setup)
    data = json.loads(sys.argv[2]);

#    for key,value in data.items():
#        print("key : ", key , "value : ",value);

    print("---------");

    for new_key,new_value in data.items():
        for org_key,org_value in setup["setup"].items():
#            print("key : ", key , "value : ",value);
            if (org_key == new_key):
                if (org_value != new_value):
                    setup["setup"][org_key]=new_value;
                    print(org_key,":", org_value ,"-->" , new_value);
        
#    print("data  message : ",data["message"]);
#    print("data  message : ",data.get("message"));
#    print("setup message : ",setup['setup']['message']);
#    print("setup message : ",setup.get('setup','message'));

#    setup["setup"]["message"]=data["message"];
    print(json.dumps(setup));
#    msg=msg.encode("utf-8");
#    print(json.dumps(setup,indent=4,sort_keys=True))
#    setup['setup']["blink"]="false";
#    print(json.dumps(setup,indent=4,sort_keys=True))
    s.sendall(json.dumps(setup).encode('UTF-8'))

#    if not (setup['setup']['type'] is None):
#        print(setup['setup']['type'])
#    else:
#        print("no value for type");
def jsetup2():
    data = json.loads(sys.argv[2]);
    print(json.dumps(data));
    s.sendall(json.dumps(data).encode('UTF-8'))

def jcall():
    call=json.loads(_call)
    data = json.loads(sys.argv[2]);

#    for key,value in data.items():
#        print("key : ", key , "value : ",value);

    print("---------");

    for new_key,new_value in data.items():
        for org_key,org_value in call["call"].items():
#            print("key : ", key , "value : ",value);
            if (org_key == new_key):
                if (org_value != new_value):
                    call["call"][org_key]=new_value;
                    print(org_key,":", org_value ,"-->" , new_value);
        
#    print("data  message : ",data["message"]);
#    print("data  message : ",data.get("message"));
#    print("call message : ",call['call']['message']);
#    print("call message : ",call.get('call','message'));

#    call["call"]["message"]=data["message"];
    print(json.dumps(call));
#    msg=msg.encode("utf-8");
#    print(json.dumps(call,indent=4,sort_keys=True))
#    call['call']["blink"]="false";
#    print(json.dumps(call,indent=4,sort_keys=True))
    s.sendall(json.dumps(call).encode('UTF-8'))

#    if not (setup['setup']['type'] is None):
#        print(setup['setup']['type'])
#    else:
#        print("no value for type");


def LINE():
    return sys._getframe(1).f_lineno

def join():
    #MCAST_GRP = '239.21.218.197'
    MCAST_GRP = '239.21.218.197'
    MCAST_PORT = 1235
    # regarding socket.IP_MULTICAST_TTL
    # ---------------------------------
    # for all packets sent, after two hops on the network the packet will not
    # be re-sent/broadcast (see https://www.tldp.org/HOWTO/Multicast-HOWTO-6.html)
    MULTICAST_TTL = 2

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, MULTICAST_TTL)

    # For Python 3, change next line to 'sock.sendto(b"robot", ...' to avoid the
    # "bytes-like object is required" msg (https://stackoverflow.com/a/42612820)
    sock.sendto(b"search", (MCAST_GRP, MCAST_PORT))
#    sock.sendall(b"get")

    data,addr = sock.recvfrom(8192)
    print("recv : ",data.decode())
    sock.close()

def loc():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((NCDB_IP, NCDB_PORT))
    msg="#loc_@#@_"+sys.argv[2];
    msg=msg.encode("utf-8");
    s.sendall(msg);
#    s.sendall(b'#loc_@#@_1')
#    data=s.recv(1024)
#    print("Received",repr(data))
    s.close()

def getPhrase():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((NCDB_IP, NCDB_PORT))
    s.sendall(b'#getPhrase_@#@_getPhrase')
#    data=s.recv(1024)
#    print("Received",repr(data))
    s.close()

def setup():
#    print("argv1 "+ sys.argv[1])
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((NCDB_IP, NCDB_PORT))
#    msg="#setup_@#@_Welcome_@#@_none_@#@_none".encode("utf-8");
    msg="#setup_@#@_"+sys.argv[2]+"_@#@_"+sys.argv[3]+"_@#@_"+sys.argv[4];
    msg=msg.encode("utf-8");
#    msg="#setup_@#@_환Welcome영_@#@_-16711681_@#@_5".encode("utf-8");
     #s.sendall(b'#setup_@#@_Welcometo SSCCTV.com_@#@_0_@#@_5')
    s.sendall(msg);
#    print("sent {(msg)}");
#    data=s.recv(1024)
#    print("Received",repr(data))
    s.close()

def em_call():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((NCDB_IP, NCDB_PORT))
    msg="#em_call_@#@_"+sys.argv[2]+"_@#@_"+sys.argv[3];
    msg=msg.encode("utf-8");
    s.sendall(msg);
#    s.sendall(b'#em_call_@#@_1_@#@_2') 1 병동 2 병실
    s.close()

def normal_call():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((NCDB_IP, NCDB_PORT))
    msg="#normal_call_@#@_"+sys.argv[2];
#    msg="#normal_call_@#@_"+sys.argv[2]+"_@#@_"+sys.argv[3];
    msg=msg.encode("utf-8");

    s.sendall(msg);
#    s.sendall(b'#normal_call_@#@_2')
    s.close()

def end():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((NCDB_IP, NCDB_PORT))
    s.sendall(b'#end')
    s.close()

def usage():
    print("./DisplayBoard_test.py [CMD]\n");
    print("[CMD] get : search DisplayBoard\n");
    print("[CMD] loc RoomA : setup location\n");
    print("[CMD] getPhrase RoomA : setup location\n");
    print("[CMD] setup message colorcode speed : ex) setup \"Welcome4 환영 긴 문자열 엄청 긴 문자열\" -16711681  7\n");
    print("[CMD] em_call 1 1 : em_call ward_number room_number\n");
    print("[CMD] normal_call 1-1-1-1 : normal_call\n");
    exit()

#def main():
#    join()
#    normal_call();    
#    print("argc = "+len(sys.argv))
#    for i,arg in enumerate(sys.argv);
#     setup();

if __name__ == "__main__":
#    print(f"argc = {len(sys.argv)}")
    if (len(sys.argv)==1):
        usage();
    if (sys.argv[1] == "join"):
        join();
    elif (sys.argv[1] == "loc"):
        if (len(sys.argv)==3):
            loc();
        else:
            usage();
    elif (sys.argv[1] == "getPhrase"):
        getPhrase();
    elif (sys.argv[1] == "setup"):
        if (len(sys.argv)==5):
            setup();
        else:
            usage();
    elif (sys.argv[1] == "em_call"):
        if (len(sys.argv)==4):
            em_call();
        else:
            usage();
    elif (sys.argv[1] == "normal_call"):
        if (len(sys.argv)==3):
            normal_call();
        else:
            usage();
    elif (sys.argv[1] == "end"):
        end();
    elif (sys.argv[1] == "jsetup"):
        jsetup();
    elif (sys.argv[1] == "jsetup2"):
        jsetup2();

    elif (sys.argv[1] == "jcall"):
        jcall();
    else:
        exit(0)



s.close()
