#!/system/bin/sh
mount -o rw,remount /system
mount -o rw,remount /system
mount -o rw,remount /system
echo "127.0.0.1       localhost
::1             ip6-localhost" > /system/etc/hosts
echo "$1 north-america.pool.ntp.org" >> /system/etc/hosts

settings put global ntp_server $1 && setprop persist.sys.timezone "Asia/Seoul" && reboot
