package com.sscctv.tempdisplayboard.fragment;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sscctv.tempdisplayboard.R;
import com.sscctv.tempdisplayboard.databinding.FragmentDisasterBinding;
import com.sscctv.tempdisplayboard.databinding.FragmentDisasterRearBinding;
import com.sscctv.tempdisplayboard.util.KeyList;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import static android.media.AudioManager.STREAM_RING;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DisasterFragment_rear#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DisasterFragment_rear extends PresentationFragment {
    private static final String TAG = DisasterFragment_rear.class.getSimpleName();

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
//    private FragmentDisasterBinding mBinding;
    private FragmentDisasterRearBinding mBinding;
    private boolean isChange = false;
    private String mParam1;
    private String getName;
    private TimerTask timerTask;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private AudioManager mAudioManager;
    private MediaPlayer mAlarm;
    JsonObject call;
    String call_message;

    public DisasterFragment_rear() {
    }

    public static DisasterFragment_rear newInstance(Context ctx, Display display , String param1, String param2) {
        DisasterFragment_rear fragment = new DisasterFragment_rear();
        fragment.setDisplay(ctx,display);
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        Log.d("DisasterFragment","newInstance");
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            getName = getArguments().getString(ARG_PARAM2);
            call = new Gson().fromJson(getName, JsonObject.class);
            //Log.d("CallFragment", "json argument : " + call.toString());
            //Log.d("CallFragment" , "call id : " + call.get("id").getAsString());
            //Log.d("CallFragment" , "call type : " + call.get("type").getAsString());
            if (call.get("cmd").getAsString().equals("start")) {
                //Log.d("CallFragment", "call message : " + call.get("call_message").getAsString());
                call_message = call.get("call_message").getAsString();
            }
        }
        Log.d("DisasterFragment_rear","mParam1 : " + mParam1);
        Log.d("DisasterFragment_rear","getName : " + getName);
//        int flag = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                | View.SYSTEM_UI_FLAG_FULLSCREEN;
//
//        flag |= View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
//        flag |= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
//        flag |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
//        getActivity().getWindow().getDecorView().setSystemUiVisibility(flag);

        DisplayMetrics metrics = new DisplayMetrics();
        DisplayMetrics real_metrics = new DisplayMetrics();
        Display display = getDisplay();
        display.getMetrics(metrics);
        display.getRealMetrics(real_metrics);
        float density = metrics.density;

        String dp;
        dp = (String.format("%dx%d",
                ((int) ((float) metrics.widthPixels / density)),
                ((int) ((float) metrics.heightPixels / density))));
        Log.d("rear : dp" , dp);

        String real_dp;
        real_dp = (String.format("%dx%d",
                ((int) ((float) real_metrics.widthPixels / density)),
                ((int) ((float) real_metrics.heightPixels / density))));
        Log.d("rear : real_dp" , real_dp);
    }


    @Override
    public void onResume() {
        super.onResume();

        mBinding.disasterMessage.setText(call_message);

        timerTask = new TimerTask() {
            @Override
            public void run() {
                mHandler.post(() -> {
                    if (!isChange) {
                        mBinding.layoutBorder.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.red));
                        isChange = true;
                    } else {
                        mBinding.layoutBorder.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.black));
                        isChange = false;
                    }
                });
            }
        };

        Timer timer = new Timer();
        timer.schedule(timerTask, 0, 100);
//        startAlarm();
    }

    @Override
    public void onPause() {
        super.onPause();
//        stopAlarm();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_disaster_rear, container, false);
        mAudioManager = ((AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE));

        Log.d("DisasterFragment_rear","onCreateView");
        mBinding.disasterMessage.setText(call_message);
        mBinding.disasterMessage.setTextSize(150);

        return mBinding.getRoot();
    }
/*
    private void startAlarm() {
        try {
            AssetFileDescriptor afd;
            afd = getContext().getAssets().openFd("a.mp3");
            if (mAlarm == null) {
                mAlarm = new MediaPlayer();
//                mAlarm.reset();
                mAlarm.setAudioStreamType(STREAM_RING);
                mAlarm.setLooping(true);
//                mAlarm.setVolume(mAudioManager.getStreamMaxVolume(STREAM_RING),mAudioManager.getStreamMaxVolume(STREAM_RING));
                try {
                    mAlarm.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                    mAlarm.prepare();
                    mAlarm.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
//                mAlarm.setOnPreparedListener(MediaPlayer::start);
//                mAlarm.prepareAsync();
//
//                mAlarm.setOnCompletionListener(mediaPlayer -> {
//                    mediaPlayer.stop();
//                    mediaPlayer.release();
//                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    private void stopAlarm() {
        if (mAlarm != null) {
            mAlarm.stop();
            mAlarm.reset();
            mAlarm.release();
            mAlarm = null;
        }
    }
 */
}