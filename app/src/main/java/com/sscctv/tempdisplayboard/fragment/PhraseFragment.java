package com.sscctv.tempdisplayboard.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.ScrollingMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.text.style.URLSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.sscctv.tempdisplayboard.MainActivity;
import com.sscctv.tempdisplayboard.R;
import com.sscctv.tempdisplayboard.databinding.FragmentPhraseBinding;
import com.sscctv.tempdisplayboard.util.CallMarquee;
import com.sscctv.tempdisplayboard.util.KeyList;
import com.sscctv.tempdisplayboard.util.TinyDB;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.security.auth.callback.Callback;

import static com.android.internal.app.IntentForwarderActivity.TAG;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PhraseFragment#newInstance} factory method to
 * create an instance of this fragment.
 */


public class PhraseFragment extends Fragment implements CallMarquee {
    private static final String TAG = PhraseFragment.class.getSimpleName();
    private TinyDB tinyDB;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private FragmentPhraseBinding mBinding;

    private String mParam1;
    private String mParam2;

    public static int BLUE          =-16776961;// (0xff0000ff)
    public static int CYAN          =-16711681;// (0xff00ffff)
    public static int DKGRAY        =-12303292;// (0xff444444)
    public static int GRAY          =-7829368;// (0xff888888)
    public static int GREEN         =-16711936;// (0xff00ff00)
    public static int LTGRAY        =-3355444;// (0xffcccccc)
    public static int MAGENTA       =-65281;// (0xffff00ff)
    public static int RED           =-65536;// (0xffff0000)
    public static int TRANSPARENT   = 0;// (0x00000000)
    public static int WHITE         =-1;// (0xffffffff)
    public static int YELLOW        =-256;// (0xffffff00)
    public static int BLACK         =-16777216;// (0xff000000)



    public PhraseFragment() {
    }
    public static PhraseFragment newInstance(String param1, String param2) {
        PhraseFragment fragment = new PhraseFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

    }

    @Override
    public void onResume() {
        super.onResume();
        String default_message = "WelCome to SeeEyes";
        int default_color = WHITE;
        float default_size = 200; // MAX : 280
//        int default_color = -16171111;
        int default_speed = 0;

//        mBinding.mv.setContent(tinyDB.getString("message"));
//        mBinding.mv.setTextColor(tinyDB.getString("color"));

        /* message */
        if (!(tinyDB.getString("message").isEmpty())) {
            default_message = tinyDB.getString("message");
        }
        mBinding.mv.setContent(default_message);

        /* color */
        if (!(tinyDB.getString("color").isEmpty()))
            default_color=Integer.parseInt(tinyDB.getString("color"));

        if (default_color == BLACK || default_color == TRANSPARENT)
            default_color=WHITE;
        mBinding.mv.setTextColor(default_color);

        /* size */
        if (!(tinyDB.getString("size").isEmpty()))
            default_size=Float.parseFloat(tinyDB.getString("size"));
//        if (default_size<100) default_size=100;
//        else if (default_size>280) default_size=280;
        mBinding.mv.setTextSize(default_size);

        /* speed */
        if (!(tinyDB.getString("speed").isEmpty()))
            default_speed=Integer.parseInt(tinyDB.getString("speed"));
        if (default_speed<0) default_speed=1;
        else if (default_speed > 10) default_speed=10;
        mBinding.mv.setTextSpeed(default_speed);
/*
        Log.d(TAG,"default_message : "+ default_message);
        Log.d(TAG,"default_color   : "+ default_color);
        Log.d(TAG,"default_speed   : "+ default_speed);
        Log.d(TAG,"default_size    : "+ default_size);

 */
        mBinding.mv.continueRoll();
    }


    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_phrase, container, false);
        tinyDB = new TinyDB(getContext());
        mBinding.mv.setListener(this);


        return mBinding.getRoot();
    }

    @Override
    public void isRunning() {
        Log.d(TAG, "MainActivity isRunning");
    }

    @Override
    public void isStop() {

        ((MainActivity) requireActivity()).setChildFragment(DateFragment.newInstance("",""), KeyList.SEND_DATE);
    }
}