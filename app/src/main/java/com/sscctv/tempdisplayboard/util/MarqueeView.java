package com.sscctv.tempdisplayboard.util;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import androidx.annotation.FontRes;
import androidx.core.content.res.ResourcesCompat;

import com.sscctv.tempdisplayboard.R;

import java.util.List;

/**
 * Created by ruedy on 2018/3/8.
 */

public class MarqueeView extends View implements Runnable {
    private static final String TAG = "MarqueeView";
    private String string;
    private float speed = 1;
    private int textColor = Color.WHITE;
    private float textSize = 200; // defined mv_text_size in attrs.xml and fragment_phrase.xml
    private int textdistance;//
    private int textDistance1 = 10;
    private String black_count = "";
    private boolean isPause = false;
    private int repetType = REPET_INTERVAL;
    public static final int REPET_ONCETIME = 0;
    public static final int REPET_INTERVAL = 1;
    public static final int REPET_CONTINUOUS = 2;

    private float startLocationDistance = 1.0f;

    private boolean isClickStop = false;
    private boolean isResetLocation = true;
    private float xLocation = 0;
    private int contentWidth;

    private boolean isRoll = false;
    private float oneBlack_width;

    private TextPaint paint;
    private Rect rect;

    private int repetCount = 0;
    private boolean resetInit = true;

    private Thread thread;
    private String content = "";

    private float textHeight;
    private CallMarquee mCallback;

    private Typeface typeface;

    public MarqueeView(Context context) {
        this(context, null);

    }

    public MarqueeView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MarqueeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttrs(attrs);
        initPaint();
        initClick();
    }

    private void initClick() {
        setOnClickListener(v -> {

            if (isClickStop) {
                if (isRoll) {
                    stopRoll();
                } else {
                    continueRoll();
                }
            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void initAttrs(AttributeSet attrs) {
        TypedArray tta = getContext().obtainStyledAttributes(attrs, R.styleable.MarqueeView);

        textColor = tta.getColor(R.styleable.MarqueeView_mv_text_color, textColor);
        isClickStop = tta.getBoolean(R.styleable.MarqueeView_mv_click_stop, isClickStop);
        isResetLocation = tta.getBoolean(R.styleable.MarqueeView_mv_is_resetLocation, isResetLocation);
        speed = tta.getFloat(R.styleable.MarqueeView_mv_text_speed, speed);
        textSize = tta.getFloat(R.styleable.MarqueeView_mv_text_size, textSize);
        textDistance1 = tta.getInteger(R.styleable.MarqueeView_mv_text_distance, textDistance1);
        startLocationDistance = tta.getFloat(R.styleable.MarqueeView_mv_text_start_loc_distance, startLocationDistance);
        repetType = tta.getInt(R.styleable.MarqueeView_mv_repeat_type, repetType);
        @FontRes int fontRes = tta.getResourceId(R.styleable.MarqueeView_mv_font, 0);
        if (fontRes != 0) {
//            Log.d(TAG, "fontRes: " + fontRes);
            typeface = ResourcesCompat.getFont(getContext(), fontRes);
        }
        tta.recycle();
    }


    /**
     * 刻字机修改
     */
    private void initPaint() {

        rect = new Rect();
        paint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL);
//        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(textColor);
//        Log.d(TAG, "Paint Align: " + paint.getTextAlign());
        paint.setTextSize(dp2px(textSize));
        if (typeface != null) {
            paint.setTypeface(typeface);
        }
    }

    public int dp2px(float dpValue) {
        final float scale = getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (resetInit) {
            setTextDistance(textDistance1);

            if (startLocationDistance < 0) {
                startLocationDistance = 0;
            } else if (startLocationDistance > 1) {
                startLocationDistance = 1;
            }
            xLocation = (getWidth() * startLocationDistance);
//            Log.e(TAG, "onMeasure: --- " + xLocation);
            resetInit = false;
        }

        switch (repetType) {
            case REPET_INTERVAL:
                if (contentWidth <= (-xLocation)) {
                    xLocation = getWidth();
                }
                break;
            case REPET_CONTINUOUS:
                if (xLocation < 0) {
                    int beAppend = (int) ((-xLocation) / contentWidth);
                    Log.e(TAG, "onDraw: ---" + contentWidth + "--------" + (-xLocation) + "------" + beAppend);
                    if (beAppend >= repetCount) {
                        repetCount++;
                        string = string + content;
                    }
                }
                break;

            default:
//                Log.d(TAG, "ContentWidth: " + contentWidth + " XLocation: " + -xLocation);
                if (contentWidth < (-xLocation)) {
                    mCallback.isStop();
                    stopRoll();
                }
                break;
        }

        if (string != null) {
            if (isPause) {
                canvas.drawText(string, canvas.getWidth() / 2, (int) ((canvas.getHeight() / 2) - (paint.descent() + paint.ascent()) / 2), paint);
            } else {
                if (isRoll) {
                    canvas.drawText(string, xLocation, (getHeight() >> 1) + textHeight / 2, paint);
                } else {
                    string = null;
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }


        }
    }

    public void setListener(CallMarquee mCallback) {
        this.mCallback = mCallback;
    }

    public void setRepetType(int repetType) {
        this.repetType = repetType;
        resetInit = true;
        setContent(content);
    }


    @Override
    public void run() {
        while (isRoll && !TextUtils.isEmpty(content)) {
            try {
                if (!isRoll) {
                }
                Thread.sleep(10);
                xLocation = xLocation - speed;
                postInvalidate();
            } catch (InterruptedException e) {
//                e.printStackTrace();
            }
        }


    }

    public void continueRoll() {
            if (!isRoll) {
                if (thread != null) {
                    thread.interrupt();
                    thread = null;
                }
                isRoll = true;
                thread = new Thread(this);
                thread.start();
            }

    }

    public void stopRoll() {
//        Log.d(TAG, "StopRoll");
        isRoll = false;
        if (thread != null) {
            thread.interrupt();
            thread = null;
        }

    }

    private boolean isRun() {
        return isRoll;
    }

    /**
     * 点击是否暂停，默认是不
     *
     * @param isClickStop
     */
    private void setClickStop(boolean isClickStop) {
        this.isClickStop = isClickStop;
    }


    /**
     * 是否循环滚动
     *
     * @param isContinuable
     */
    private void setContinueble(int isContinuable) {
        this.repetType = isContinuable;
    }

    /**
     * 设置文字间距  不过如果内容是List形式的，该方法不适用 ,list的数据源，必须在设置setContent之前调用此方法。
     *
     * @param textdistance2
     */
    public void setTextDistance(int textdistance2) {

        String black = " ";
        oneBlack_width = getBlacktWidth();
        textdistance2 = dp2px(textdistance2);
        int count = (int) (textdistance2 / oneBlack_width);
        if (count == 0) {
            count = 1;
        }
        textdistance = (int) (oneBlack_width * count);
        black_count = "";
        for (int i = 0; i <= count; i++) {
            black_count = black_count + black;
        }
        setContent(content);
    }

    /**
     * 计算出一个空格的宽度
     *
     * @return
     */
    private float getBlacktWidth() {
        String text1 = "en en";
        String text2 = "enen";
        return getContentWidth(text1) - getContentWidth(text2);

    }

    private float getContentWidth(String black) {
        if (black == null || black.equals("")) {
            return 0;
        }

        if (rect == null) {
            rect = new Rect();
        }
        paint.getTextBounds(black, 0, black.length(), rect);
        textHeight = getContentHeight();

        return rect.width();
    }

    private float getContentHeight() {
        Paint.FontMetrics fontMetrics = paint.getFontMetrics();
        return Math.abs((fontMetrics.bottom - fontMetrics.top)) / 2;
    }

    public void setTextColor(int textColor) {
        if (textColor != 0) {
            this.textColor = textColor;
            paint.setColor(textColor);
        }
    }

    public void setTextSize(float textSize) {
        if (textSize > 0) {
            this.textSize = textSize;
            paint.setTextSize(dp2px(textSize));
            contentWidth = (int) (getContentWidth(content) + textdistance);
        }
    }
    public void setTextAlign(boolean mode) {
        if(mode) {
            paint.setTextAlign(Paint.Align.CENTER);
        } else {
            paint.setTextAlign(Paint.Align.LEFT);
        }
    }

    public void setTextSpeed(float speed) {
        this.speed = speed;
    }

    public void setContent(List<String> strings) {
        setTextDistance(textDistance1);
        StringBuilder temString = new StringBuilder();

        if (strings != null && strings.size() != 0) {
            for (int i = 0; i < strings.size(); i++) {
                temString.append(strings.get(i)).append(black_count);
            }
        }
        setContent(temString.toString());
    }

    public void setContent(String content2) {
        if (TextUtils.isEmpty(content2)) {
            return;
        }
        if (isResetLocation) {
            xLocation = getWidth() * startLocationDistance;
        }

        if (!content2.endsWith(black_count)) {
            content2 = content2 + black_count;
        }
        this.content = content2;

        if (repetType == REPET_CONTINUOUS) {
            contentWidth = (int) (getContentWidth(content) + textdistance);
            repetCount = 0;
            int contentCount = (getWidth() / contentWidth) + 2;
            this.string = "";
            for (int i = 0; i <= contentCount; i++) {
                this.string = String.format("%s%s", this.string, this.content);
            }
        } else {
            if (xLocation < 0 && repetType == REPET_ONCETIME) {
                if (-xLocation > contentWidth) {
                    xLocation = getWidth() * startLocationDistance;
                }
            }
            contentWidth = (int) getContentWidth(content);
            this.string = content2;
        }
//        Log.d(TAG, "getContentWidth: " + contentWidth);
        if (contentWidth > 1860) {
            contentWidth = contentWidth - 1860;
            isPause = false;
        } else {
            isPause = true;
        }
        setTextAlign(isPause);
        if (!isRoll) {
            continueRoll();
        }


    }

    private void setResetLocation(boolean isReset) {
        isResetLocation = isReset;
    }

    public void appendContent(String appendContent) {
    }


}
