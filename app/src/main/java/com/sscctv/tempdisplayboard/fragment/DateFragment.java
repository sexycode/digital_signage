package com.sscctv.tempdisplayboard.fragment;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sscctv.tempdisplayboard.MainActivity;
import com.sscctv.tempdisplayboard.R;
import com.sscctv.tempdisplayboard.databinding.FragmentDateBinding;
import com.sscctv.tempdisplayboard.util.KeyList;

import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import com.sscctv.tempdisplayboard.util.TinyDB;

import static com.android.internal.app.IntentForwarderActivity.TAG;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DateFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DateFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static TinyDB tinyDB;
    private FragmentDateBinding mBinding;
    private long dateTime;
    private boolean isChange = false;
    private TimerTask timerTask;
    private Handler mHandler = new Handler(Looper.getMainLooper());

    private String mParam1;
    private String mParam2;
    private int count = 0;
    private static boolean ampm_visible=true; // default format is ampm display
    float default_size = 240;

    public DateFragment() {
    }

    public static DateFragment newInstance(String param1, String param2) {
        DateFragment fragment = new DateFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        // App Crash test
        // System.out.println(1/0);
    }

    @Override
    public void onResume() {
        super.onResume();

        timerTask = new TimerTask() {
            @Override
            public void run() {
                mHandler.post(() -> {
                    count ++;
                    if (count == 12) { // 6 초
                        ((MainActivity) requireActivity()).setChildFragment(PhraseFragment.newInstance("",""), "");
                        count = 0;
                    }
                    if(mBinding.layoutPhrase.getVisibility() == View.INVISIBLE) {
                        mBinding.layoutPhrase.setVisibility(View.VISIBLE);
                    }

                    if(!isChange) {
                        mBinding.colon.setVisibility(View.INVISIBLE);
                        isChange = true;
                    } else {
                        mBinding.colon.setVisibility(View.VISIBLE);
                        isChange = false;
                    }
                });
                mHandler.post(mUpdateTimeTask);
            }
        };

        Timer timer = new Timer();
        timer.schedule(timerTask, 0, 500);
    }

    @Override
    public void onPause() {
        super.onPause();

        if(timerTask != null) {
            timerTask.cancel();
            timerTask = null;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_date, container, false);
        tinyDB = new TinyDB(getContext());
        mBinding.layoutPhrase.setVisibility(View.INVISIBLE);


        String ampm=tinyDB.getString("date_format");


        if (ampm.length()>0) {
           if (ampm.equals("12")) { ampm_visible=true; }
           else if (ampm.equals("24")) { ampm_visible=false; }
        }
        else ampm_visible=true;
        // 왜 컴파일 에러가 안나지?
//        if (ampm.length()>0 && ampm=="24")  { Log.d(TAG,"2--> ampm_visible = false"); ampm_visible=false; }
        /* size */
        if (!(tinyDB.getString("size").isEmpty()))
            default_size=Float.parseFloat(tinyDB.getString("size"));
//        if (default_size<100) default_size=100;
//        else if (default_size>280) default_size=280;

        return mBinding.getRoot();
    }

    private Runnable mUpdateTimeTask = new Runnable() {

        public void run() {
            dateTime = System.currentTimeMillis();
            Date date = new Date(dateTime);

            SimpleDateFormat fHour;
            if (ampm_visible==true) fHour = new SimpleDateFormat("hh", Locale.KOREA);
            else fHour = new SimpleDateFormat("kk", Locale.KOREA);
            SimpleDateFormat fMinute = new SimpleDateFormat("mm", Locale.KOREA);
            SimpleDateFormat fMonth = new SimpleDateFormat("M", Locale.KOREA);
            SimpleDateFormat fDay = new SimpleDateFormat("dd", Locale.KOREA);
            SimpleDateFormat fAa = new SimpleDateFormat("aa", Locale.ENGLISH);

            String strMonth = fMonth.format(date);
            String strDay = fDay.format(date);
            String strHour = fHour.format(date);
            String strMinute = fMinute.format(date);
            String strAa= fAa.format(date);

            if (ampm_visible==false) {
                mBinding.ampm.setVisibility(View.INVISIBLE);
                mBinding.ampm.setText("");
            }
            else {
                mBinding.ampm.setVisibility(View.VISIBLE);
                if (strAa.equals("AM")) mBinding.ampm.setText("오전");
                else if (strAa.equals("PM")) mBinding.ampm.setText("오후");
            }

            mBinding.month.setText(strMonth);
            mBinding.day.setText(strDay);
            mBinding.hour.setText(strHour);
            mBinding.minute.setText(strMinute);

            mBinding.month.setTextSize(default_size);
            mBinding.day.setTextSize(default_size);
            mBinding.hour.setTextSize(default_size);
            mBinding.minute.setTextSize(default_size);

            mBinding.txtDay.setTextSize(default_size/2);
            mBinding.txtMonth.setTextSize(default_size/2);
            mBinding.ampm.setTextSize(default_size/2);
            mBinding.colon.setTextSize((float) (default_size*0.7));
        }
    };
}