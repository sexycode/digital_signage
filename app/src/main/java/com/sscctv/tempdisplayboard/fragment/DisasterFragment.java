package com.sscctv.tempdisplayboard.fragment;

import android.annotation.TargetApi;
import android.app.Presentation;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRouter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sscctv.tempdisplayboard.R;
import com.sscctv.tempdisplayboard.databinding.FragmentDisasterBinding;
import com.sscctv.tempdisplayboard.util.KeyList;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import static android.content.Context.MEDIA_ROUTER_SERVICE;
import static android.media.AudioManager.STREAM_RING;
import static androidx.core.content.ContextCompat.getSystemService;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DisasterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DisasterFragment extends PresentationFragment {
    private static final String TAG = DisasterFragment.class.getSimpleName();

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private FragmentDisasterBinding mBinding;
//    private FragmentCallBinding mBinding;
    private boolean isChange = false;
    private String mParam1;
    private String getName;
    private TimerTask timerTask;
    Timer timer = null;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private AudioManager mAudioManager;
    private MediaPlayer mAlarm;
    JsonObject call;
    String call_message;

    /* presentation config */
    MediaRouter router = null;
    MediaRouter.SimpleCallback cb = null;
    PresentationFragment preso = null;
    View inline = null;
    TextView mv = null;
    Display display = null;
    DisasterFragment_rear a = null;

    public DisasterFragment() {
    }

    public static DisasterFragment newInstance(Context ctx,Display display ,String param1, String param2) {
        DisasterFragment fragment = new DisasterFragment();
        fragment.setDisplay(ctx,display);
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        Log.d("DisasterFragment","newInstance");
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            getName = getArguments().getString(ARG_PARAM2);
            call = new Gson().fromJson(getName, JsonObject.class);
            //Log.d("CallFragment", "json argument : " + call.toString());
            //Log.d("CallFragment" , "call id : " + call.get("id").getAsString());
            //Log.d("CallFragment" , "call type : " + call.get("type").getAsString());
            if (call.get("cmd").getAsString().equals("start")) {
                //Log.d("CallFragment", "call message : " + call.get("call_message").getAsString());
                call_message = call.get("call_message").getAsString();
            }
        }
        Log.d("DisasterFragment","mParam1 : " + mParam1);
        Log.d("DisasterFragment","getName : " + getName);

        // presentation setup
        if (cb==null) {
            cb = new RouteCallback();
            router = (MediaRouter) getActivity().getSystemService(MEDIA_ROUTER_SERVICE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

//        mBinding.disasterMessage.setText(getName);
        timerTask = new TimerTask() {
            @Override
            public void run() {
                mHandler.post(() -> {
                    if (!isChange) {
                        mBinding.layoutBorder.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.red));
                        isChange = true;
                    } else {
                        mBinding.layoutBorder.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.black));
                        isChange = false;
                    }
                });
            }
        };

        timer = new Timer();
        timer.schedule(timerTask, 0, 100);
        startAlarm();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopAlarm();
        timer.cancel();
        timer.purge();
        timer = null;
        timerTask.cancel();
        timerTask = null;
        mHandler = null;
        Log.d("DisasterFragment","onPause()1");
        if (a !=  null)
            a.onPause();
        Log.d("DisasterFragment","onPause()2");
        router.removeCallback(cb);


        Log.d("DisasterFragment","onPause()3");
        clearPreso(false);

        Log.d("DisasterFragment","onPause()end");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_disaster, container, false);
        mAudioManager = ((AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE));

        Log.d("DisasterFragment","onCreateView");
        handleRoute(router.getSelectedRoute(MediaRouter.ROUTE_TYPE_LIVE_VIDEO));
        router.addCallback(MediaRouter.ROUTE_TYPE_LIVE_VIDEO, cb);
        mBinding.disasterMessage.setText(call_message);
        /*
        if (getName.contains("#disaster_call")) {
            String[] emCall = getName.split(KeyList.STRING_SPLIT);
            String prefix = emCall[0];
            String msg= emCall[1];

            Log.d("Disaster onCreateView"," : " + prefix +":"+ msg);
            msg = "<-" + msg;
            mBinding.disasterMessage.setText(msg);
        }
        else
        {
            Log.d("CallFragment","unknown : " + getName);
        }

         */

        DisplayMetrics metrics = new DisplayMetrics();
        Display display = getDisplay();
        display.getMetrics(metrics);
        float density = metrics.density;

        String dp;
        dp = (String.format("%dx%d",
                ((int) ((float) metrics.widthPixels / density)),
                ((int) ((float) metrics.heightPixels / density))));
        Log.d("front" , dp);
        return mBinding.getRoot();
    }

    private void startAlarm() {
        try {
            AssetFileDescriptor afd;
            afd = getContext().getAssets().openFd("disaster_call.mp3");
            if (mAlarm == null) {
                mAlarm = new MediaPlayer();
//                mAlarm.reset();
                mAlarm.setAudioStreamType(STREAM_RING);
                mAlarm.setLooping(true);
//                mAlarm.setVolume(mAudioManager.getStreamMaxVolume(STREAM_RING),mAudioManager.getStreamMaxVolume(STREAM_RING));
                try {
                    mAlarm.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                    mAlarm.prepare();
                    mAlarm.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
//                mAlarm.setOnPreparedListener(MediaPlayer::start);
//                mAlarm.prepareAsync();
//
//                mAlarm.setOnCompletionListener(mediaPlayer -> {
//                    mediaPlayer.stop();
//                    mediaPlayer.release();
//                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopAlarm() {
        if (mAlarm != null) {
            mAlarm.stop();
            mAlarm.reset();
            mAlarm.release();
            mAlarm = null;
        }
    }

    private void handleRoute(MediaRouter.RouteInfo route) {
        if (route == null) {
            clearPreso(true);
        }
        else {
            display=route.getPresentationDisplay();
            Log.d("handleRoute" , "display.getDisplayId() = " + display.getDisplayId());
            if (route.isEnabled() && display != null) {
                if (preso == null) {
                    showPreso(route);
                    Log.d(getClass().getSimpleName(), "enabled route");
                }
                else if (preso.getDisplay().getDisplayId() != display.getDisplayId()) {
                    clearPreso(true);
                    showPreso(route);
                    Log.d(getClass().getSimpleName(), "switched route");
                }
                else {
                    // no-op: should already be set
                }
            }
            else {
                clearPreso(true);
                Log.d(getClass().getSimpleName(), "disabled route");
            }
        }
    }

    private void clearPreso(boolean switchToInline) {
        if (switchToInline) {
            inline.setVisibility(View.VISIBLE);
            mv.setText("clearPreso");
            getActivity().getSupportFragmentManager().beginTransaction()
                    .add(R.id.phrase, buildPreso(null)).commit();
        }

        if (preso != null) {
            preso.dismiss();
            preso=null;
        }
    }

    private void showPreso(MediaRouter.RouteInfo route) {
        /*
        if (inline.getVisibility() == View.VISIBLE) {
            inline.setVisibility(View.GONE);
            mv.setText("Rear content");

            Fragment f=getSupportFragmentManager().findFragmentById(R.id.phrase);

            getSupportFragmentManager().beginTransaction().remove(f).commit();
        }
         */

        preso=buildPreso(route.getPresentationDisplay());
        preso.show(getActivity().getSupportFragmentManager(), "preso");
    }

    private PresentationFragment buildPreso(Display display) {
        Log.d("buildPreso","default display " + Display.DEFAULT_DISPLAY);
//    return(SamplePresentationFragment.newInstance(this, display, "https://commonsware.com"));
        Log.d("buildPreso" , "display = " + display.getDisplayId());

        a = DisasterFragment_rear.newInstance(getContext(), display,"rear",getName);
        return(a);
    }

    private class RouteCallback extends MediaRouter.SimpleCallback {
        @Override
        public void onRoutePresentationDisplayChanged(MediaRouter router,
                                                      MediaRouter.RouteInfo route) {
            handleRoute(route);
        }
    }

}