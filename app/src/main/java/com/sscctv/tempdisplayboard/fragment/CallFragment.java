package com.sscctv.tempdisplayboard.fragment;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sscctv.tempdisplayboard.R;
import com.sscctv.tempdisplayboard.databinding.FragmentCallBinding;
import com.sscctv.tempdisplayboard.util.CallList;
import com.sscctv.tempdisplayboard.util.Detail_Message;
import com.sscctv.tempdisplayboard.util.TinyDB;

import java.io.IOException;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static android.media.AudioManager.STREAM_RING;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CallFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CallFragment extends Fragment {
    private static final String TAG = CallFragment.class.getSimpleName();

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static TinyDB tinyDB;
    private FragmentCallBinding mBinding;
    private boolean isChange = false;
    private String mParam1;
    private String getName;
    private TimerTask timerTask;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private AudioManager mAudioManager;
    private MediaPlayer mAlarm;
    JsonObject call;

    CallList myCallList = CallList.getInstance();

    int volume=1;
    Integer call_type=0; // 1 : em , 2  : normal
    String call_message;

    public CallFragment() {
    }
    public void onStop() {
        super.onStop();
    }
    public static CallFragment newInstance(String param1, String param2) {
        CallFragment fragment = new CallFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            getName = getArguments().getString(ARG_PARAM2);
            call = new Gson().fromJson(getName, JsonObject.class);
            //Log.d("CallFragment", "json argument : " + call.toString());
            //Log.d("CallFragment" , "call id : " + call.get("id").getAsString());
            //Log.d("CallFragment" , "call type : " + call.get("type").getAsString());
            if (call.get("cmd").getAsString().equals("start")) {
                //Log.d("CallFragment", "call message : " + call.get("call_message").getAsString());
                call_message = call.get("call_message").getAsString();
            }
            if (call.get("type").getAsString().equals("em")) call_type=1;
            else if (call.get("type").getAsString().equals("normal")) call_type=2;
            else if (call.get("type").getAsString().equals("disaster")) call_type=3;
        }
    }

    int index_em=0;
    public String get_EM_Message()
    {
//        myCallList.getCount();
        int i;
        String msg=null;
        Detail_Message item;
        //Log.d("get_EM_Message","index_em : " + index_em + " getEm_count : " + myCallList.getEm_count());
        if (index_em>=myCallList.getEm_count()) index_em=0;

        if (myCallList.getEm_count()>0)
        {
            call_type=1;
            item= myCallList.get_EM_Data2(index_em);
            if (item != null) {
                msg = item.getMessage();
            }
            else
                Log.d("item  is NULL","msg is null");
            index_em++;
        }
        return msg;
    }
    int index_normal;

    public String get_NORMAL_Message()
    {
//        myCallList.getCount();
        int i;
        String msg=null;
        Detail_Message item;
//        Log.d("get_NORMAL_Message","index_normal : " + index_normal + " getNormal_count : " + myCallList.getNormal_count());
        if (index_normal>=myCallList.getNormal_count()) index_normal=0;

        if (myCallList.getNormal_count()>0)
        {
            call_type=2;
            item= myCallList.get_NORMAL_Data2(index_normal);
            if (item != null)
                msg=item.getMessage();
            else
                Log.d("item  is NULL","msg is null");
            index_normal++;
        }
        return msg;
    }
    /*
    public String get_NORMAL_Message()
    {
//        myCallList.getCount();
        int i;
        String msg="Not set";
        Map<String,String> detail;
        Detail_Message item;
//        Set<Map.Entry<String, String>> detail;
        if (index_normal>=myCallList.getNormal_count()) index_normal=0;

        if (myCallList.getNormal_count()>0)
        {
            for (i=index_normal;i<myCallList.getNormal_count();i++)
            {
                item= myCallList.get_NORMAL_Data2(i);
                Log.d("NORMAL_Message ---->" , "index_normal : " + index_normal + "type : " + item.getType() + " message : " + item.getMessage());
                msg=item.getMessage();
                index_normal++;
                break;
            }
        }
        return msg;
    }
     */

    @Override
    public void onResume() {
        super.onResume();

        Runnable msg_change = new Runnable() {
            int i=0;


            @Override
            public void run() {
                i++;
                /*
                String display_msg=get_EM_Message();
                if (display_msg == null || display_msg.length()==0)
                {
                    Log.d("display_msg" , "EM not found");
//                    display_msg = get_NORMAL_Message();
                }
                else
                    Log.d("display_msg : " , display_msg);
                 */
                if (!isChange) {
                    String display_msg = get_EM_Message();
                    if (display_msg == null || display_msg.length()==0)
                    {
//                        Log.d("display_msg" , "EM not found");
                        display_msg = get_NORMAL_Message();
                    }
                    else
                        Log.d("display_msg : " , display_msg + " length : " + display_msg.length() );
                    float diff_msg_size = display_msg.length()-10;
                    mBinding.txtRoomNum.setText(display_msg);

//                    if (diff_msg_size>0) {
//                        float cal = 200-(200*(diff_msg_size/15));
//                        Log.d("size" , "differ = " + diff_msg_size + " : size " + cal);
//                        mBinding.txtRoomNum.setTextSize(cal);
//                    }
                    if (call_type==1) {
                        mBinding.layoutBorder.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.red));
                    } else if (call_type==2){
                        mBinding.layoutBorder.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.orange));
                    }
                    else {
                        mBinding.layoutBorder.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorAccent));
                    }

                    isChange = true;
                } else {
                    mBinding.layoutBorder.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.black));
                    isChange = false;
                }
            }
        };

        timerTask = new TimerTask() {
            @Override
            public void run() {
                    mHandler.post(msg_change);
/*
                    mHandler.post(() -> {
                        if (!isChange) {
                            if (getName.contains("#em_call")) {
                                mBinding.layoutBorder.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.red));
                            } else {
                                mBinding.layoutBorder.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.orange));
                                mBinding.txtRoomNum.setText(String.format("%d:%02d", minutes, seconds));
                            }
                            isChange = true;
                        } else {
                            mBinding.layoutBorder.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.black));
                            isChange = false;
                        }
                    });
 */
            }
        };

        Timer timer = new Timer();
        timer.schedule(timerTask, 0, 500);
        startAlarm();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopAlarm();
        timerTask.cancel();
        call_type=0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_call, container, false);
        mAudioManager = ((AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE));
        tinyDB = new TinyDB(getContext());
        volume= Integer.parseInt(tinyDB.getString("volume"));


//        myCallList.getCount2();
/*
        if (call.get("type").getAsString().equals("em")) {
            Log.d("CallFragment","em_call : " + call.get("id").getAsString()+":"+ call.get("call_message").getAsString());
            mBinding.txtRoomNum.setText(call.get("call_message").getAsString());
//          em_call_list.put(call.get("id").getAsString(),call.get("call_message").getAsString());
            call_type=1;
        } else if (call.get("type").getAsString().equals("normal")) {
            Log.d("CallFragment","normal_call : " + call.get("id").getAsString()+":"+ call.get("call_message").getAsString());
            mBinding.txtRoomNum.setText(call.get("call_message").getAsString());
            call_type=2;
        } else if (call.get("type").getAsString().equals("end")) {
            Log.d("CallFragment","end: " );
            call_type=0;
        }
        else
        {
            Log.d("CallFragment","unknown type : " + call.get("type").getAsString());
        }
 */
        return mBinding.getRoot();
    }

    private void startAlarm() {

        try {
            AssetFileDescriptor afd;
            switch (call_type)
            {
                case 1: // em_sound
                    afd = getContext().getAssets().openFd("em_call.mp3");
                    break;
                case 2: // normal_sound
                    afd = getContext().getAssets().openFd("normal_call.mp3");
                    break;
                default:
                    afd = getContext().getAssets().openFd("normal_call.mp3");
            }

            if (mAlarm == null) {
                mAlarm = new MediaPlayer();
//                mAlarm.reset();
                mAlarm.setAudioStreamType(STREAM_RING);
                mAlarm.setLooping(true);
                mAudioManager.setStreamVolume(STREAM_RING,volume,0);
//                int currentVolume =mAudioManager.getStreamVolume(AudioManager.STREAM_RING);
//                Log.d("startAlarm" , "current Volume : " +currentVolume);

//                mAlarm.setVolume(mAudioManager.getStreamMaxVolume(STREAM_RING),mAudioManager.getStreamMaxVolume(STREAM_RING));
                try {
                    mAlarm.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                    mAlarm.prepare();
                    mAlarm.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
//                mAlarm.setOnPreparedListener(MediaPlayer::start);
//                mAlarm.prepareAsync();
//
//                mAlarm.setOnCompletionListener(mediaPlayer -> {
//                    mediaPlayer.stop();
//                    mediaPlayer.release();
//                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void stopAlarm() {
        if (mAlarm != null) {
            mAlarm.stop();
            mAlarm.reset();
            mAlarm.release();
            mAlarm = null;
        }
    }
}