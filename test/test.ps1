function normal_em_test($count)
{
	# normal start
	for ($i=0;$i -lt $count ; $i++)
	{

	$id = "id_$i"
	$normal_msg = "normal call $i"
	$normal_call = '{\"call\": { \"type\" : \"normal\" ,\"cmd\":\"start\", \"id\" : '
	$normal_idm = '\"' + $id + '\",'
	$normal_msgm = '\"call_message\" : \"' + $normal_msg+ '\"}}'

	$normal_json = $($normal_call+$normal_idm+$normal_msgm)
	Write-Output $normal_json

	python.exe .\DisplayBoard_test_json.py jsetup2 $normal_json
	$delay = $($i+2)
	Start-Sleep -Seconds 1
	}

	Start-Sleep -Seconds $count

	# em 
	em_test $count

	#end
	for ($i=0;$i -lt $count ; $i++)
	{

	$id = "id_$i"
	$end_call = '{\"call\": { \"type\" : \"normal\" , \"cmd\":\"stop\",\"id\" : '
	$end_idm = '\"' + $id + '\"}}'



	$end_json = $($end_call+$end_idm)
	Write-Output $end_json

	python.exe .\DisplayBoard_test_json.py jsetup2 $end_json
	Start-Sleep -Seconds 1
	}
}
function normal_test($count)
{
	for ($i=0;$i -lt $count ; $i++)
	{

	$id = "id_$i"
	$normal_msg = "normal call $i"
	$normal_call = '{\"call\": { \"type\" : \"normal\" ,\"cmd\":\"start\", \"id\" : '
	$normal_idm = '\"' + $id + '\",'
	$normal_msgm = '\"call_message\" : \"' + $normal_msg+ '\"}}'

	$normal_json = $($normal_call+$normal_idm+$normal_msgm)
	Write-Output $normal_json

	python.exe .\DisplayBoard_test_json.py jsetup2 $normal_json
	$delay = $($i+2)
	Start-Sleep -Seconds 1
	}

	Start-Sleep -Seconds $count
	

	#end
	for ($i=0;$i -lt $count ; $i++)
	{

	$id = "id_$i"
	$end_call = '{\"call\": { \"type\" : \"normal\" , \"cmd\":\"stop\",\"id\" : '
	$end_idm = '\"' + $id + '\"}}'



	$end_json = $($end_call+$end_idm)
	Write-Output $end_json

	python.exe .\DisplayBoard_test_json.py jsetup2 $end_json
	Start-Sleep -Seconds 1
	}
}
function em_normal_test($count)
{
	for ($i=0;$i -lt $count ; $i++)
	{

	$id = "id_$i"
	$normal_msg = "EM call $i"
	$normal_call = '{\"call\": { \"type\" : \"em\" ,\"cmd\":\"start\", \"id\" : '
	$normal_idm = '\"' + $id + '\",'
	$normal_msgm = '\"call_message\" : \"' + $normal_msg+ '\"}}'

	$normal_json = $($normal_call+$normal_idm+$normal_msgm)
	Write-Output $normal_json

	python.exe .\DisplayBoard_test_json.py jsetup2 $normal_json
	$delay = $($i+2)
	Start-Sleep -Seconds 1
	}

	Start-Sleep -Seconds $count

	# normal test : 표시되지 않음 
	normal_test $count

	#end
	for ($i=0;$i -lt $count ; $i++)
	{

	$id = "id_$i"
	$end_call = '{\"call\": { \"type\" : \"em\" , \"cmd\":\"stop\",\"id\" : '
	$end_idm = '\"' + $id + '\"}}'



	$end_json = $($end_call+$end_idm)
	Write-Output $end_json

	python.exe .\DisplayBoard_test_json.py jsetup2 $end_json
	Start-Sleep -Seconds 1
	}
}

function em_test($count)
{
	for ($i=0;$i -lt $count ; $i++)
	{

	$id = "id_$i"
	$normal_msg = "EM call $i"
	$normal_call = '{\"call\": { \"type\" : \"em\" ,\"cmd\":\"start\", \"id\" : '
	$normal_idm = '\"' + $id + '\",'
	$normal_msgm = '\"call_message\" : \"' + $normal_msg+ '\"}}'

	$normal_json = $($normal_call+$normal_idm+$normal_msgm)
	Write-Output $normal_json

	python.exe .\DisplayBoard_test_json.py jsetup2 $normal_json
	$delay = $($i+2)
	Start-Sleep -Seconds 1
	}

	Start-Sleep -Seconds $count
	

	#end
	for ($i=0;$i -lt $count ; $i++)
	{

	$id = "id_$i"
	$end_call = '{\"call\": { \"type\" : \"em\" , \"cmd\":\"stop\",\"id\" : '
	$end_idm = '\"' + $id + '\"}}'



	$end_json = $($end_call+$end_idm)
	Write-Output $end_json

	python.exe .\DisplayBoard_test_json.py jsetup2 $end_json
	Start-Sleep -Seconds 1
	}
}


function looptest($count)
{
	for ($i=0;$i -lt $count ; $i++)
	{

	$id = "id_$i"
	$normal_msg = "normal call $i"
	$normal_call = '{\"call\": { \"type\" : \"normal\" ,\"cmd\":\"start\", \"id\" : '
	$normal_idm = '\"' + $id + '\",'
	$normal_msgm = '\"call_message\" : \"' + $normal_msg+ '\"}}'

	$i++
	$id = "id_$i"
	
	$em_msg = "em call $i"
	$em_call = '{\"call\": { \"type\" : \"em\" ,\"cmd\":\"start\", \"id\" : '
	$em_idm = '\"' + $id + '\",'
	$em_msgm = '\"call_message\" : \"' + $em_msg+ '\"}}'



	$normal_json = $($normal_call+$normal_idm+$normal_msgm)
	$em_json = $($em_call+$em_idm+$em_msgm)
	Write-Output $normal_json
	Write-Output $em_json


	python.exe .\DisplayBoard_test_json.py jsetup2 $normal_json
	Start-Sleep -Seconds 5
	python.exe .\DisplayBoard_test_json.py jsetup2 $em_json
	Start-Sleep -Seconds 5
	}

	#end
	for ($i=0;$i -lt $count ; $i++)
	{

	$id = "id_$i"
	$end_call = '{\"call\": { \"type\" : \"end\" ,\"cmd\":\"stop\", \"id\" : '
	$end_idm = '\"' + $id + '\"}}'



	$end_json = $($end_call+$end_idm)
	Write-Output $end_json


	python.exe .\DisplayBoard_test_json.py jsetup2 $end_json
	Start-Sleep -Seconds 5
	}
}

function normal_em_disaster_disaster_stop_em_stop_normal_stop()
{
	python.exe .\DisplayBoard_test_json.py jsetup2 '{\"call\": { \"type\" : \"normal\" , \"cmd\": \"start\", \"id\": \"id_aaa\", \"call_message\" : \"normal call\"}}'
	Start-Sleep -Seconds 2
	python.exe .\DisplayBoard_test_json.py jsetup2 '{\"call\": { \"type\" : \"em\" , \"cmd\": \"start\", \"id\": \"id_aaa\", \"call_message\" : \"em call\"}}'
	Start-Sleep -Seconds 2
	python.exe .\DisplayBoard_test_json.py jsetup2 '{\"call\": { \"type\" : \"disaster\" , \"cmd\": \"start\", \"id\": \"id_aaa\", \"call_message\" : \"Disaster\"}}'
	Start-Sleep -Seconds 2

	python.exe .\DisplayBoard_test_json.py jsetup2 '{\"call\": { \"type\" : \"disaster\" , \"cmd\": \"stop\", \"id\": \"id_aaa\"}}'
	Start-Sleep -Seconds 2
	python.exe .\DisplayBoard_test_json.py jsetup2 '{\"call\": { \"type\" : \"em\" , \"cmd\": \"stop\", \"id\": \"id_aaa\"}}'
	Start-Sleep -Seconds 2
	python.exe .\DisplayBoard_test_json.py jsetup2 '{\"call\": { \"type\" : \"normal\" , \"cmd\": \"stop\", \"id\": \"id_aaa\"}}'
}
#for ($i = 1; $i -lt 5; $i++) {
#  Write-Output "$i Test"
##  Start-Sleep -Seconds $i
	normal_em_test 100
	em_normal_test 100
	em_test 100
	normal_test 100
	normal_em_disaster_disaster_stop_em_stop_normal_stop
#}