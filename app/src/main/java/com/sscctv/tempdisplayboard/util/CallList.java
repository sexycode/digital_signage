package com.sscctv.tempdisplayboard.util;

import android.util.Log;

import com.sscctv.tempdisplayboard.MainActivity;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;




public class CallList {
    private static CallList instance=null;
//    private static Map<String ,String> call_list = new LinkedHashMap<>();
//    private static Map<String ,Map<String,String>> call_list1 = new LinkedHashMap<>();
    private static final Map<String , Detail_Message> em_list= new LinkedHashMap<>();
    private static final Map<String , Detail_Message> normal_list= new LinkedHashMap<>();
    private static final Map<String , Detail_Message> disaster_list= new LinkedHashMap<>();

    private int em_count;
    private int normal_count;
    private int disaster_count;

    private CallList(){}
/*
    public void setData(String key , String value) {
        call_list.put(key,value);
    }
    public String getData(int index) {
        return call_list.get((call_list.keySet().toArray())[index]);
    }

 */
    public  void remove_All() {
        em_list.clear();
        normal_list.clear();
        disaster_list.clear();
    }
    public int remove_EM_Data(String key) {
        int ret;
        if (em_list.remove(key) == null) {
            ret = -1;
        }
        else ret = 0;
        return ret;
    }
    public int remove_NORMAL_Data(String key) {
        int ret;
        if (normal_list.remove(key) == null) {
            Log.d("remove_NORMAL_Data" , "fail to remove in normal_list");
            ret = -1;
        }
        else ret = 0;
        return ret;
    }
    public int remove_DISASTER_Data(String key) {
        int ret=0;
        disaster_list.clear();
        /*
        if (disaster_list.remove(key) == null) {
            ret = -1;
        }
        else ret = 0;
        */
        return ret;
    }
    /*
    public String getData(String key) {
        return call_list.get(key);
    }

     */

    /* Map in Map get/set */
    /*
    public void setData1(String key,String type , String message) {
        Map<String,String> msg = new LinkedHashMap<>();
        msg.put(type,message);
        call_list1.put(key,msg);
        getCount();
    }
    public Map<String,String> getData1(int index) {

        Map<String,String> msg=call_list1.get((call_list.keySet().toArray())[index]);
        return msg;
    }

     */
/*
    public void getCount()
    {
        em_count=normal_count=disaster_count=0;
//        for (i=0;i<getSize();i++)
//        {
//            Map<String,String> msg=call_list1.get((call_list.keySet().toArray())[i]);
//            Log.d("getCount","msg values " + msg.values());
//        }
//        for (String key: call_list1.keySet())
//        {
//            Log.d("getCount","key : " + key + "/ value :" + call_list1.get(key));
//        }

        for (Map.Entry<String, Map<String, String>> outer : Collections.unmodifiableSet(call_list1.entrySet()))
        {
            Log.d("getCount" , "key  = " + outer.getKey() + " : value = "+ outer.getValue());
//            Map.Entry<String, String> detail = (Map.Entry<String, String>) outer.getValue().entrySet();
//            Log.d("allValues detail" , "key  = " + detail.getKey() + " : value = "+ detail.getValue());
            for (Map.Entry<String, String> detail : Collections.unmodifiableSet(outer.getValue().entrySet()))
            {
                Log.d("getCount detail" , "key  = " + detail.getKey() + " : value = "+ detail.getValue());
                if (detail.getKey().equals("em")) em_count++;
                else if (detail.getKey().equals("normal")) normal_count++;
                else if (detail.getKey().equals("disaster")) disaster_count++;
            }
        }
    }

 */
    /*
    public void getCount2()
    {
        em_count=normal_count=disaster_count=0;
//        for (i=0;i<getSize();i++)
//        {
//            Map<String,String> msg=call_list1.get((call_list.keySet().toArray())[i]);
//            Log.d("getCount","msg values " + msg.values());
//        }
//        for (String key: call_list1.keySet())
//        {
//            Log.d("getCount","key : " + key + "/ value :" + call_list1.get(key));
//        }

        for (Map.Entry<String, Detail_Message> outer : Collections.unmodifiableSet(call_list2.entrySet()))
        {
            Detail_Message detail= null;
            detail = outer.getValue();
            if (detail.getType().equals("em")) em_count++;
            else if (detail.getType().equals("normal")) normal_count++;
            else if (detail.getType().equals("disaster")) disaster_count++;
        }
    }

     */
    public int getEm_count(){
        return em_list.size();
    }
    public int getNormal_count() {
        return normal_list.size();
    }
    public int getDisaster_count() {
        return disaster_list.size();
    }
/*
    public int getSize() {
       return call_list.size();
    }
*/
    /* Details get/set */
    public void setData2(String key , String type,String message) {
        Detail_Message msg = new Detail_Message(type,message);
        if (type.equals("em")) em_list.put(key,msg);
        else if (type.equals("normal")) normal_list.put(key,msg);
        else if (type.equals("disaster")) disaster_list.put(key,msg);
//        call_list2.put(key,msg);
//        Log.d("set_Data2 ------->"  , ": " + call_list2.get(key).toString());
//        getCount2();
    }
    /*
    public Detail_Message getData2(String key) {
        Detail_Message item = call_list2.get("1");
        return item;
    }

     */
    /*
    public Detail_Message getData2(int index,String type) {
        int i=0;
        Detail_Message ret = null;
        for (Map.Entry<String, Detail_Message> outer : Collections.unmodifiableSet(call_list2.entrySet()))
        {
            Log.d("getData2" , "index = " + i);
//            Map.Entry<String, String> detail = (Map.Entry<String, String>) outer.getValue().entrySet();
            if (i==index ) {
                ret = outer.getValue();
                if (ret.getType().equals(type)) {
                    Log.d("same type", "key  = " + ret.getType() + " : value = " + ret.getMessage());
                    return ret;
                }
                else
                    Log.d("not type", "key  = " + ret.getType() + " : value = " + ret.getMessage());
            }
            i++;
        }
        Log.d("getData2" , "End");
        return ret;
    }

     */
    public Detail_Message get_EM_Data2(int index) {
        int i=0;
        Detail_Message ret = null;
        for (Map.Entry<String, Detail_Message> outer : Collections.unmodifiableSet(em_list.entrySet()))
        {
//            Log.d("getData2" , "index = " + index);
//            Map.Entry<String, String> detail = (Map.Entry<String, String>) outer.getValue().entrySet();
            if (i==index ) {
                ret = outer.getValue();
//                Log.d("same type", "key  = " + ret.getType() + " : value = " + ret.getMessage());
                break;
//                return ret;
            }
            else
                i++;
        }
//        Log.d("getData2" , "End");
        return ret;
    }
    public Detail_Message get_NORMAL_Data2(int index) {
        int i=0;
        Detail_Message ret = null;
        for (Map.Entry<String, Detail_Message> outer : Collections.unmodifiableSet(normal_list.entrySet()))
        {
//            Map.Entry<String, String> detail = (Map.Entry<String, String>) outer.getValue().entrySet();
            if (i==index ) {
                ret = outer.getValue();
//                Log.d("same type", "key  = " + ret.getType() + " : value = " + ret.getMessage());
                return ret;
            }
            i++;
        }
        return ret;
    }


    public static synchronized CallList getInstance() {
        if (instance == null) {
            instance = new CallList();
        }
        return  instance;
    }
}