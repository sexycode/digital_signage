package com.sscctv.tempdisplayboard.util;

public class KeyList {
    public static final String STRING_SPLIT= "_@#@_";
    public static final String MODEL_NAME= "NCDB";


    public static final String KEY_SETUP_LOCATION = "setup_location";
    public static final String KEY_STRING_PHRASE = "string_phrase";
    public static final String KEY_STRING_COLOR = "string_color";
    public static final String KEY_STRING_SPEED = "string_speed";

    public static final String SEND_NORMAL_CALL = "call";
    public static final String SEND_EM_CALL = "em_call";
    public static final String SEND_DISASTER_CALL = "disaster_call";
    public static final String SEND_DATE = "date";
    public static final String SEND_PHRASE = "phrase";
    public static final String SEND_WAIT = "wait";

    public static final String MODEL_NCPB = "ncpb";        // 콘솔형 간호사호출기 2P
    public static final String MODEL_NCPE = "ncpe";        // 콘솔형 간호사호출기 3P
    public static final String MODEL_NCPBW = "ncpbw";        // 벽부형 간호사호출기 2P
    public static final String MODEL_NCPBS = "ncpbs";        // 단독형 간호사호출기

    public static final String MODEL_NCSEB = "ncseb";        // 위급호출기 버튼형
    public static final String MODEL_NCSEL = "ncsel";        // 위급호출기 당김형
}
