package com.sscctv.tempdisplayboard;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.media.AudioManager;
import android.net.EthernetManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import com.sscctv.tempdisplayboard.BuildConfig;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sscctv.tempdisplayboard.databinding.ActivityMainBinding;
import com.sscctv.tempdisplayboard.fragment.CallFragment;
import com.sscctv.tempdisplayboard.fragment.DisasterFragment;
import com.sscctv.tempdisplayboard.fragment.PhraseFragment;
import com.sscctv.tempdisplayboard.util.CallList;
import com.sscctv.tempdisplayboard.util.FTPsscctv;
import com.sscctv.tempdisplayboard.util.KeyList;
import com.sscctv.tempdisplayboard.util.RootShell;
import com.sscctv.tempdisplayboard.util.TinyDB;
import com.sscctv.tempdisplayboard.util.Utils;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Method;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TimerTask;

import static java.net.InetAddress.getByName;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String TIME_SERVER ="192.168.50.5";
    private ActivityMainBinding mBinding;
    private Handler mHandler = new Handler();
    private TinyDB tinyDB;

    private static final int tcpPort = 59009;
    private static final int mPort = 1235;
    private static final String mIp = "239.21.218.197";
    private static final String sIp = "239.21.218.198";
    private static final String timeServerIp = "192.168.50.5";
    private boolean isRunning = true;
    private TimerTask timerTask;
    private boolean isChange = false;
    private boolean isOpen = true;
    private EthernetManager mEthernetManager;

    CallList call_List = CallList.getInstance();


    Utils utils = new Utils();

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
/*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Settings.System.canWrite(this)) {
                Toast.makeText(this, "onCreate: Already Granted", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "onCreate: Not Granted. Permission Requested", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS);
                intent.setData(Uri.parse("package:" + this.getPackageName()));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }
*/

        //
//        startActivity(new Intent(android.provider.Settings.ACTION_DATE_SETTINGS));
//        getCurrentNetworkTime();
        // ntp
//        byte[] timeServer = new byte[]{(byte)192,(byte)168,(byte)50,(byte)5};
/*
        NTPUDPClient timeClient = new NTPUDPClient();
        InetAddress timeServer = null;
        try {
            timeServer = getByName(timeServerIp);
            Log.d(TAG,"aaaa");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        Log.d(TAG,"bbb");
        TimeInfo timeInfo = null;
        try {
            timeInfo = timeClient.getTime(timeServer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        long returnTime = timeInfo.getReturnTime();

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(returnTime);
*/

        Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this,MainActivity.class));

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        tinyDB = new TinyDB(this);

        mBinding.drawerLayout.setOnTouchListener(this);
        Log.d(TAG, "Mode: " + utils.getEthMode());
        Log.d(TAG, "Serial: " + utils.getSerialNumber());
        Log.d(TAG, "Ethernet: " + utils.getIPAddress(true));
        Log.d(TAG, "Mac: " + utils.getMacAddress("eth0"));

        mBinding.txtLoc.setText(tinyDB.getString(KeyList.KEY_SETUP_LOCATION));
        mBinding.txtIp.setText(utils.getIPAddress(true));
    }








    public static long getCurrentNetworkTime() {
        long returnTime = 0;
        NTPUDPClient timeClient = new NTPUDPClient();
        timeClient.setDefaultTimeout(1000);
        // Connect to network. Try again on timeout (max 6).
        for (int retries = 7; retries >= 0; retries--) {

            try {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                // Try connecting to network to get time. May timeout.
                InetAddress inetAddress = InetAddress.getByName(TIME_SERVER);
                TimeInfo timeInfo = timeClient.getTime(inetAddress);
                long networkTimeLong = timeInfo.getMessage().getTransmitTimeStamp().getTime();

                // Convert long to Date, and Date to String. Log results.
                Date networkTimeDate = new Date(networkTimeLong);
                Log.i("Time", "Time from " + TIME_SERVER + ": " + networkTimeDate);
                returnTime = networkTimeDate.getTime();
                break;
                // Return resulting time as a String.
            } catch (IOException e) {
                // Max of 6 retries.
                Log.i("RTCTestActivity", "Unable to connect. Retries left: " + (retries - 1));
            }
        }
        Log.d(TAG,"TIME ---> "+returnTime);

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(returnTime);
        return returnTime;
    }



    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume()");
//        TaskMultiCastServer(new MultiCastServer());
        TaskTcpServer(new TcpServer());
        changeFragment(KeyList.SEND_PHRASE, "");

//        timerTask = new TimerTask() {
//            @Override
//            public void run() {
//
//                if (isOpen) {
//                    mHandler.post(() -> {
//                        if (!isChange) {
//                            changeFragment(key.SEND_DATE);
//                            isChange = true;
//                        } else {
//                            changeFragment(key.SEND_PHRASE);
//                            isChange = false;
//                        }
//                    });
//                }
//
//            }
//        };
//
//        Timer timer = new Timer();
//        timer.schedule(timerTask, 0, 5000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause()");
//        if(timerTask != null) {
//            timerTask.cancel();
//            timerTask = null;
//        }
        isRunning = false;
    }


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        Log.d(TAG, "Event: " + motionEvent.getAction());
        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
            isOpen = mBinding.menu.getVisibility() != View.VISIBLE;
        }
        return false;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        Log.d(TAG, "KeyUp: " + event);
        return false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.d(TAG, "KeyDown: " + event);
        return false;
    }

    private void TaskMultiCastServer(MultiCastServer multiCastServer) {
        multiCastServer.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @SuppressLint("StaticFieldLeak")
    private class MultiCastServer extends AsyncTask<Void, String, String> {

        protected String doInBackground(Void... params) {
            try {
                Log.w(TAG, "<< ...MultiCast Server running... >>");
//                ServerSocket serverSocket = new ServerSocket(tcpPort);
                MulticastSocket socket = new MulticastSocket(mPort);
                InetAddress byName = getByName(mIp);
                socket.joinGroup(byName);

                while (true) {
                    byte[] buf = new byte[8192];
                    DatagramPacket packet = new DatagramPacket(buf, buf.length);
                    socket.receive(packet);
                    String data = new String(packet.getData(), 0, packet.getLength());
                    String[] strings = data.split(KeyList.STRING_SPLIT);
                    Log.d(TAG, "Received: " + data);
                    if (strings[0].equals("search")) {

                        Map<String, ?> allEntries = tinyDB.getAll();
                        JsonObject getall= new JsonObject();

                        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
//                            Log.d("map values", entry.getKey() + ": " + entry.getValue().toString());
                            getall.addProperty(entry.getKey(),entry.getValue().toString());
                        }
                        getall.addProperty("version",BuildConfig.VERSION_NAME);
                        JsonObject res = new JsonObject();
                        res.addProperty("getall",getall.toString());

                        String outString=res.toString();
                        Log.d("Response : " , outString);

                        buf = outString.getBytes();
                        InetAddress client = getByName(sIp);
                        packet = new DatagramPacket(buf, buf.length, client, mPort);
                        socket.send(packet);

                       /*
                        JsonObject join= new JsonObject();

                        join.addProperty("model",getModel());
                        join.addProperty("MAC",getMacAddress("eth0"));
                        join.addProperty("IP",getIPAddress(true));
                        join.addProperty("location",getLocation());


                        JsonObject res = new JsonObject();
                        res.addProperty("get",join.toString());
                        Log.d(TAG,"join : "+join.toString());
                        InetAddress client = getByName(sIp);
                        String str=res.toString();
                        buf = str.getBytes();
                        packet = new DatagramPacket(buf, buf.length, client, mPort);
                        socket.send(packet);
                        */
/*
                        InetAddress client = getByName(sIp);
                        String str = "get" + KeyList.STRING_SPLIT + getModel();
                        str += KeyList.STRING_SPLIT + getMacAddress("eth0");
                        str += KeyList.STRING_SPLIT + getIPAddress(true);
                        str += KeyList.STRING_SPLIT + getLocation();
                        buf = str.getBytes();
                        packet = new DatagramPacket(buf, buf.length, client, mPort);
                        socket.send(packet);
                        Log.d(TAG, client + " send Data: " + str);
 */
                    } else if (data.contains("close")) {
                        socket.close();
                    }
//
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "What: " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.w(TAG, "<< ...Multicast Service End... >>");

        }
    }

    private void TaskTcpServer(TcpServer tcpServer) {
        tcpServer.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @SuppressLint("StaticFieldLeak")
    private class TcpServer extends AsyncTask<Void, String, String> {
        Utils util = new Utils();
        private FTPsscctv ftp;
        //Variable to store brightness value
        private int brightness;
        //Content resolver used as a handle to the system's settings
        private ContentResolver cResolver;
        //Window object, that will store a reference to the current window
        private Window window;

        @RequiresApi(api = Build.VERSION_CODES.M)
        protected String doInBackground(Void... params) {
            try {
                Log.w(TAG, "<< ...TCP Server running... >>");
                ServerSocket serverSocket = new ServerSocket();
                serverSocket.setReuseAddress(true);
                serverSocket.bind(new InetSocketAddress(tcpPort));

                while (isRunning) {
                    Log.w(TAG, "<< ...Waiting for client connection... >>");
                    Socket sock = serverSocket.accept();
                    Log.i(TAG, "<< ...TCP Client IP: " + sock.getInetAddress() + " connected... >>");
                    if (sock.getInputStream() != null) {
                        try {
                            BufferedReader in = new BufferedReader(new InputStreamReader(sock.getInputStream(), StandardCharsets.UTF_8));
                            String str = in.readLine();

                            String outString = null;
                            Log.d(TAG, "Get String1: " + str);
                            if (str!=null) {
                                if (str.contains("getall")) { // getall protocol
                                    Map<String, ?> allEntries = tinyDB.getAll();
                                    JsonObject getall= new JsonObject();

                                    for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
                                        Log.d("map values", entry.getKey() + ": " + entry.getValue().toString());
                                        getall.addProperty(entry.getKey(),entry.getValue().toString());
                                    }
                                    getall.addProperty("version",BuildConfig.VERSION_NAME);
                                    JsonObject res = new JsonObject();
                                    res.addProperty("getall",getall.toString());

                                    BufferedWriter out = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
                                    outString=res.toString();
                                    out.write(Objects.requireNonNull(outString));
                                    Log.d(TAG,"response : " + outString);
                                    out.close();
                                }
                                else if (str.contains("get")) { // get protocol
                                    JsonObject get = new Gson().fromJson(str, JsonObject.class).getAsJsonObject("get");

                                    Log.d(TAG,"get : "+get.toString());
                                    util.jsonGetDB(tinyDB,get);

                                    BufferedWriter out = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
                                    String get_data  = get.toString();

                                    JsonObject res = new JsonObject();
                                    res.addProperty("get",get_data);

                                    outString=res.toString();
                                    out.write(Objects.requireNonNull(outString));
                                    Log.d(TAG,"response : " + outString);
                                    out.close();
                                }
                                else if (str.contains("\"set\"")) { // set protocol
                                    JsonObject setup = new Gson().fromJson(str, JsonObject.class).getAsJsonObject("set");

                                    util.jsonPutDB(tinyDB,setup);
                                    if (util.jsonCheck(setup, "ntp_ip") == 0) {
                                        Log.d("ntp_ip", setup.get("ntp_ip").getAsString());
//                                        utils.setNtpServer(setup.get("ntp_ip").getAsString());
                                        utils.setNTP1(setup.get("ntp_ip").getAsString());
//                                        utils.sscctvReboot();
                                    }
                                    if (util.jsonCheck(setup, "volume") == 0) {
                                        Log.d("volume", setup.get("volume").getAsString());
                                        AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                                        int currentVolume = audio.getStreamVolume(AudioManager.STREAM_RING);
                                        int maxVolume = audio.getStreamMaxVolume(AudioManager.STREAM_RING);
//                                        float percent = 0.7f;
                                        short seventyVolume = (short) setup.get("volume").getAsShort();
                                        Log.d("volume", "current_volume " + currentVolume);
                                        Log.d("volume", "maxVolume  " + maxVolume);
                                        audio.setStreamVolume(AudioManager.STREAM_RING, (int) seventyVolume, 0);

                                        Log.d("debug ", "volume = " + tinyDB.getString("volume"));
                                    }
                                    getDataProcess(KeyList.SEND_PHRASE, "refresh");
                                }
                                else if (str.contains("\"call\"")) { // call protocol
                                    JsonObject call = new Gson().fromJson(str, JsonObject.class).getAsJsonObject("call");

                                    String type;

                                    if (util.jsonCheck(call,"type")!=0) {
                                        Log.d(TAG, "call : type is Error");
                                        continue;
                                    }
                                    type = call.get("type").getAsString();

                                    if (type.equals("em")) {
                                        if ( util.jsonCheck(call,"id")==0 && util.jsonCheck(call,"cmd")==0) {
                                            if (call.get("cmd").getAsString().equals("start")) {
                                                if (util.jsonCheck(call, "call_message")==0 ) {
                                                    call_List.setData2(call.get("id").getAsString(), call.get("type").getAsString(), call.get("call_message").getAsString());
                                                    getJDataProcess(KeyList.SEND_EM_CALL, call);
                                                }
                                                else {
                                                    Log.d(TAG, "call_message is not found");
                                                }
                                            }
                                            else if (call.get("cmd").getAsString().equals("stop")) {
                                                getJDataProcess(KeyList.SEND_PHRASE, call);
                                            }
                                            else
                                                Log.d(TAG, "cmd is Error");
                                        } else
                                            Log.d(TAG, "id||cmd is not found");
                                    } else if (type.equals("normal")) {
                                        if ( util.jsonCheck(call,"id")==0 && util.jsonCheck(call,"cmd")==0) {
                                            if (call.get("cmd").getAsString().equals("start")) {
                                                 if (util.jsonCheck(call,"call_message")==0 ) {
                                                     call_List.setData2(call.get("id").getAsString(), call.get("type").getAsString(), call.get("call_message").getAsString());
                                                     getJDataProcess(KeyList.SEND_NORMAL_CALL, call);
                                                 }
                                                 else {
                                                     Log.d(TAG, "call_message is not found");
                                                 }
                                            }
                                            else if (call.get("cmd").getAsString().equals("stop")) {
                                                getJDataProcess(KeyList.SEND_PHRASE, call);
                                            }
                                            else
                                                Log.d(TAG, "cmd is Error");
                                        } else
                                            Log.d(TAG, "id||cmd is not found");
                                    } else if (type.equals("disaster")) {
                                        if (util.jsonCheck(call,"id")==0 && util.jsonCheck(call,"cmd")==0 ) {
                                            if (call.get("cmd").getAsString().equals("start")) {
                                                if (util.jsonCheck(call,"call_message")==0){
                                                    call_List.setData2(call.get("id").getAsString(), call.get("type").getAsString(), call.get("call_message").getAsString());
                                                    getJDataProcess(KeyList.SEND_DISASTER_CALL, call);
                                                }
                                                else {
                                                    Log.d(TAG, "call_message is not found");
                                                }
                                            }
                                            else if (call.get("cmd").getAsString().equals("stop")) {
                                                getJDataProcess(KeyList.SEND_PHRASE, call);
                                            }
                                            else
                                                Log.d(TAG, "cmd is Error");
                                        }
                                        else
                                            Log.d(TAG, "id||cmd is not found");

                                    } else if (type.equals("all")) {
                                        if (call.get("cmd").getAsString().equals("stop"))
                                        {
                                            call_List.remove_All();
                                            changeFragment(KeyList.SEND_PHRASE, "");
                                        }
                                    } else {
                                        Log.d(TAG, "call : type is not found " + type);
                                        continue;
                                    }
                                }
                                else if (str.contains("\"debug\"")) { // sscctv debug protocol
                                    JsonObject debug = new Gson().fromJson(str, JsonObject.class).getAsJsonObject("debug");
                                    if (util.jsonCheck(debug,"volume")==0) {
                                        Log.d("volume" , debug.get("volume").getAsString());
                                        AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                                        int currentVolume = audio.getStreamVolume(AudioManager.STREAM_RING);
                                        int maxVolume = audio.getStreamMaxVolume(AudioManager.STREAM_RING);
//                                        float percent = 0.7f;
                                        short seventyVolume = (short) debug.get("volume").getAsShort();
                                        Log.d("volume" ,"current_volume " + currentVolume);
                                        Log.d("volume" ,"maxVolume  " + maxVolume);
                                        audio.setStreamVolume(AudioManager.STREAM_RING, (int)seventyVolume, 0);

                                        Log.d("debug " , "volume = " + tinyDB.getString("volume"));
                                    }
                                    else if (util.jsonCheck(debug,"brightness")==0) {
                                        Log.d("brightness" , debug.get("brightness").getAsString());
                                        cResolver = getApplicationContext().getContentResolver();
                                        brightness = Settings.System.getInt(cResolver, Settings.System.SCREEN_BRIGHTNESS);

                                        Log.d("brightness" , "1current brightness " + brightness);
                                        setBrightness(debug.get("brightness").getAsInt());
                                        brightness = Settings.System.getInt(cResolver, Settings.System.SCREEN_BRIGHTNESS);
                                        Log.d("brightness" , "2current brightness " + brightness);
                                    }
                                }
                                else if (str.contains("\"update\"")) {
                                    JsonObject update = new Gson().fromJson(str, JsonObject.class).getAsJsonObject("update");
                                    String command;
                                    command = update.get("command").getAsString();
                                    Log.d(TAG, "command -> " + command);

                                    if (command.equals("app")) {
                                        if (util.jsonCheck(update,"update_file_name")==0 &&
                                                util.jsonCheck(update,"ftp_ip")==0 ) {
                                            Log.d(TAG,"update app");
                                            boolean downStatus=false;
                                            ftp = new FTPsscctv();
                                            try {
                                                boolean ftpStatus = ftp.ftpConnect(update.get("ftp_ip").getAsString(), update.get("ftp_id").getAsString(), update.get("ftp_pass").getAsString(), update.get("ftp_port").getAsInt());
                                                if (ftpStatus == true)
                                                    Log.d(TAG, "ftp connect OK");
                                                else {
                                                    Log.d(TAG, "ftp connect failed");
                                                    continue;
                                                }
                                            } catch (Exception e) {
                                                Log.d(TAG,e.getMessage());
                                            }


//                                            String newFilePath = Environment.getDataDirectory().getAbsolutePath();
                                            String newFilePath = Environment.getExternalStorageDirectory().getAbsolutePath();
//                                            String newFilePath = getApplicationContext().getFilesDir().getPath();

//                                            String state = Environment.getExternalStorageState();
//
//                                            // Check if writable
//                                            if (Environment.MEDIA_MOUNTED.equals(state)) {
//                                                if(Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
//                                                    Log.d(TAG,"Read Only");
//                                                }else{
//                                                    Log.d(TAG,"Write OK");
//                                                }
//                                            }

                                            File file;
                                            newFilePath += "/new.apk";
                                            Log.d(TAG,"new file path " + newFilePath);
                                            try  {
                                                file = new File(newFilePath);
                                                file.createNewFile();
                                            } catch (Exception e) {
                                                Log.d(TAG,"create file fail" + e.getMessage());
                                            }
//                                            util.isStoragePermissionGranted(MainActivity.this);


                                            downStatus = ftp.ftpDownloadFile(ftp.ftpGetDirectory()+"/"+update.get("update_file_name").getAsString() , newFilePath);
                                            if (downStatus== true) {
                                                Log.d(TAG, "ftp download OK");
                                                utils.InstallAPK(newFilePath);
//                                                utils.RestartApp(MainActivity.this);
                                            }
                                            else
                                                Log.d(TAG,"ftp download failed");

                                        }
                                    }
                                    else if (command.equals("ipaddress")) {
                                        if (util.jsonCheck(update,"new_ip")==0 &&
                                                util.jsonCheck(update,"new_netmask")==0 &&
                                                util.jsonCheck(update,"new_gateway")==0
                                        ) {
                                            Log.d(TAG, "update ipaddress");

                                            Log.d(TAG,"new_ip " + update.get("new_ip").getAsString());
                                            Log.d(TAG,"new_mask " + update.get("new_netmask").getAsString());
                                            Log.d(TAG,"new_gw " + update.get("new_gateway").getAsString());
                                            Log.d(TAG,"new_dns  " + update.get("new_dns").getAsString());
                                            util.saveNetConfig(update.get("new_ip").getAsString(),
                                                    update.get("new_netmask").getAsString(),
                                                    update.get("new_gateway").getAsString(),
                                                    update.get("new_dns").getAsString());
                                        }
                                    }
                                    else if (command.equals("reboot")) {
                                        Log.d(TAG, "reboot SCDB");
                                        utils.sscctvReboot();
                                    }
                                    else
                                        Log.d(TAG, "update command not found ->" + command);
                                }
                                else
                                {
                                    Log.d("TCP Server" , "Unknown protocol : " + str);
                                    continue;
                                }
                            }
/*
                            if (str.contains("#loc")) {
                                BufferedWriter out = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));

                                String[] loc = str.split(KeyList.STRING_SPLIT);
                                tinyDB.putString(KeyList.KEY_SETUP_LOCATION, loc[1]);
                                outString = loc[1];

                                Log.d(TAG,"loc "+ loc[1]);
                                out.write(Objects.requireNonNull(outString));
                                out.close();
                            } else if(str.equals("#getPhrase")) {
                                BufferedWriter out = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));

                                outString = tinyDB.getString(KeyList.KEY_STRING_PHRASE);
                                Log.d(TAG,"getPhrase "+outString);

                                out.write(Objects.requireNonNull(outString));
                                out.close();
                            } else if(str.contains("#setup")) {
                                BufferedWriter out = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));

                                String[] phrase = str.split(KeyList.STRING_SPLIT);
                                if(!phrase[1].equals("none")) {

                                    Log.d(TAG,"string -> "+phrase[1]);
                                    tinyDB.putString(KeyList.KEY_STRING_PHRASE, phrase[1]);
                                }

                                if(!phrase[2].equals("none")) {
                                    Log.d(TAG,"color -> "+phrase[2]);
                                    tinyDB.putInt(KeyList.KEY_STRING_COLOR, Integer.parseInt(phrase[2]));
                                }

                                if(!phrase[3].equals("none")) {
                                    Log.d(TAG,"speed -> "+phrase[3]);
                                    tinyDB.putInt(KeyList.KEY_STRING_SPEED, Integer.parseInt(phrase[3]));
                                }
                                outString = tinyDB.getString(KeyList.KEY_STRING_PHRASE);
                                getDataProcess(KeyList.SEND_PHRASE,"refresh");
                                out.write(Objects.requireNonNull(outString));
                                out.close();
                            } else if(str.contains("#em_call")) {
                                getDataProcess(KeyList.SEND_EM_CALL,str);
                            } else if(str.contains("#normal_call")) {
                                getDataProcess(KeyList.SEND_NORMAL_CALL, str);
                            } else if(str.contains("#end")) {
                                getDataProcess(KeyList.SEND_PHRASE,"end");
                            }
*/

                        } catch (Exception e)
                        {
                            Log.d(TAG,"<< TCP Server ERROR " + e.getMessage());
                        }
                        finally {
                            Log.w(TAG, "<< ...TCP Client IP: " + sock.getInetAddress().getHostAddress() + " disconnected... >>");
                            sock.close();
                        }
                    }
                }
                Log.d(TAG, "<< ...TCP Server stop... >>");
                serverSocket.close();
            } catch (Exception e) {
                Log.d(TAG,"TCP server EXCEPTION -------->"+e.getMessage());
                e.printStackTrace();
            } finally {
                Log.d(TAG,"TCP server finally -------->");
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.w(TAG, "<< ...TCP Service End... Restart >>");
            // 아래는 address already in use 에러가 나옴
            // socket 은 닫았으나 timed_wait 가 있어서 reuse 가 안됨
            // TaskTcpServer(new TcpServer());
        }
    }

    public void setBrightness(int brightness){

        //constrain the value of brightness
        if(brightness < 0)
            brightness = 0;
        else if(brightness > 255)
            brightness = 255;

        ContentResolver cResolver = this.getApplicationContext().getContentResolver();
        Settings.System.putInt(cResolver, Settings.System.SCREEN_BRIGHTNESS, brightness);
    }


    private void getJDataProcess(String mode, JsonObject jsonObject) {
        final String TAG = MainActivity.class.getSimpleName();
        mHandler.post(() -> {
            switch (mode) {
                case KeyList.SEND_PHRASE:
//                    if(jsonObject.equals("refresh")) {
//                        refreshFragment(new PhraseFragment());
//                    }
                    if (jsonObject.get("cmd").getAsString().equals("stop"))
                    {
                        Log.d("getJDataProcess" , "json " + jsonObject.toString());
                        Log.d("getJDataProcess" , "id = " + jsonObject.get("id").getAsString());
                        Log.d("getJDataProcess" , "type = " + jsonObject.get("type").getAsString());


                        String _type=jsonObject.get("type").getAsString();

                        if (_type.equals("em") && call_List.getEm_count()>0)
                        {
                            if (call_List.remove_EM_Data(jsonObject.get("id").getAsString())==0) {
                                if (call_List.getEm_count()>0) {
                                    changeFragment(KeyList.SEND_EM_CALL, jsonObject.toString());
                                }
                                else {
                                    if (call_List.getNormal_count()>0)
                                        changeFragment(KeyList.SEND_NORMAL_CALL, jsonObject.toString());
                                    else
                                        changeFragment(KeyList.SEND_PHRASE, "");
                                }
                            }
                            else Log.d(TAG,"em_list remove failed");
                        }
                        else if (_type.equals("normal") && call_List.getNormal_count()>0)
                        {
                            if (call_List.remove_NORMAL_Data(jsonObject.get("id").getAsString())==0) {
                                if (call_List.getNormal_count()>0) {
                                    changeFragment(KeyList.SEND_NORMAL_CALL, jsonObject.toString());
                                }
                                else
                                    if (call_List.getEm_count()>0)
                                        changeFragment(KeyList.SEND_EM_CALL, jsonObject.toString());
                                    else
                                        changeFragment(KeyList.SEND_PHRASE, "");
                            }
                            else Log.d(TAG,"normal_list remove failed");
                        }
                        else if (_type.equals("disaster") && call_List.getDisaster_count()>0)
                        {
                            if (call_List.remove_DISASTER_Data(jsonObject.get("id").getAsString())==0) {
                                // disaster 는 clear 하기 때문에 아래 코드는 진입하지 않음
                                // 다른 코드와 유사성을 지키기 위해 유지함
                                if (call_List.getDisaster_count()>0) {
                                    changeFragment(KeyList.SEND_DISASTER_CALL, jsonObject.toString());
                                }
                                else {
                                    if (call_List.getEm_count()>0) {
                                        changeFragment(KeyList.SEND_EM_CALL, jsonObject.toString());
                                    }
                                    else {
                                        if (call_List.getNormal_count()>0)
                                            changeFragment(KeyList.SEND_NORMAL_CALL, jsonObject.toString());
                                        else
                                            changeFragment(KeyList.SEND_PHRASE, "");
                                    }
                                }
                            }
                            else Log.d(TAG,"disaster_list remove failed");
                        }
                        else
                        {
                            Log.d(TAG,"type is unknown or count is 0");
                        }

                        /*
                        if (call_List.getEm_count()>0 || call_List.getNormal_count()>0 || call_List.getDisaster_count()>0) {
                            call_List.removeData(jsonObject.get("id").getAsString());
                            changeFragment(KeyList.SEND_EM_CALL, jsonObject.toString());
                        }
                        else if (call_List.getDisaster_count()>0) {
                            call_List.removeData(jsonObject.get("id").getAsString());
                            changeFragment(KeyList.SEND_PHRASE, "");
                        }
                        else {
                            Log.d("end" , "PhraseFragment");
                            changeFragment(KeyList.SEND_PHRASE, "");
                        }
                         */
                    }

                    break;
                case KeyList.SEND_EM_CALL:
                    Log.d(TAG,"getJDataProcess : " + jsonObject.toString());
                    changeFragment(KeyList.SEND_EM_CALL, jsonObject.toString());
                    break;
                case KeyList.SEND_NORMAL_CALL:
                    Log.d(TAG,"getJDataProcess : " + jsonObject.toString());
                    changeFragment(KeyList.SEND_NORMAL_CALL, jsonObject.toString());
                    break;
                case KeyList.SEND_DISASTER_CALL:
                    Log.d(TAG,"getJDataProcess : " + jsonObject.toString());
                    changeFragment(KeyList.SEND_DISASTER_CALL, jsonObject.toString());
                    break;
            }
//            changeFragment(str);
        });
    }


    private void getDataProcess(String mode, String str) {
        mHandler.post(() -> {
            switch (mode) {
                case KeyList.SEND_PHRASE:
                    if(str.equals("refresh")) {
                        refreshFragment(new PhraseFragment());
                    } else if(str.equals("end")) {
                        changeFragment(KeyList.SEND_PHRASE, "");
                    }
                    break;
                    /*
                case KeyList.SEND_EM_CALL:
                    Log.d(TAG,"getDataProcess : " + str);
                    changeFragment(KeyList.SEND_EM_CALL, str);
                    break;
                case KeyList.SEND_NORMAL_CALL:
                    Log.d(TAG,"getDataProcess : " + str);
                    changeFragment(KeyList.SEND_NORMAL_CALL, str);
                    break;
                case KeyList.SEND_DISASTER_CALL:
                    Log.d(TAG,"getDataProcess : " + str);
                    changeFragment(KeyList.SEND_DISASTER_CALL, str);
                    break;
                    */
            }
//            changeFragment(str);
        });
    }


    private void changeFragment(String mode, String str) {
        Fragment fragment = null;

        if (mode.equals(KeyList.SEND_NORMAL_CALL)) {
            fragment = CallFragment.newInstance(mode, str);
        } else if(mode.equals(KeyList.SEND_EM_CALL)) {
            // 여기서 call id , call_message 를 함께 넘긴다.
            fragment = CallFragment.newInstance(mode, str);
        } else if(mode.equals(KeyList.SEND_DISASTER_CALL)) {
            Log.d("changeFragment" , "DisasterFragment.newInstance");

            //handleRoute(router.getSelectedRoute(MediaRouter.ROUTE_TYPE_LIVE_VIDEO));
            //router.addCallback(MediaRouter.ROUTE_TYPE_LIVE_VIDEO, cb);
            fragment = DisasterFragment.newInstance(this, getWindowManager().getDefaultDisplay(), "changeFragment",str);
        } else {
            Log.d("changeFragment" , "PhraseFragment.newInstance");
            fragment = PhraseFragment.newInstance("", "");
        }
        setChildFragment(fragment, mode);
    }

    public void setChildFragment(Fragment child, String mode) {
        FragmentTransaction childFt = getSupportFragmentManager().beginTransaction();
//        childFt.setCustomAnimations(R.anim.anim_fade_in, R.anim.anim_fade_out);

        if (!child.isAdded()) {
            childFt.replace(R.id.child_settings_fragment, child);
            childFt.addToBackStack(null);
            childFt.commit();
        }
    }

    public void refreshFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.detach(fragment).attach(fragment).commit();
    }



//    private String getIpAddress() {
//        String address;
//
//        if(getEthMode()){
//            try {
//                Class<?> c = Class.forName("android.os.SystemProperties");
//                Method get = c.getMethod("get", String.class);
//                address = (String) get.invoke(c, "dhcp.eth0.ipaddress");
//            } catch (Exception e) {
//                e.printStackTrace();
//                address = null;
//            }
//        } else {
//
//        }
//
//
//        return address;
//    }

/*
    private static String getIPAddress() {
        String sValue = "";

        try {
            Process p = Runtime.getRuntime().exec("ifconfig eth0");
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            sValue = input.readLine();
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(sValue != null) {
            if (sValue.contains("ip")) {
                sValue = sValue.substring(sValue.indexOf("ip ") + 3, sValue.indexOf(" mask"));
            }
        } else {
            sValue = "null";
        }
        Log.d("IP","ipaddress ---->"+sValue);
        return sValue;
    }

    private static String getMacAddress() {
        String sValue = "";

        try {
            Process p = Runtime.getRuntime().exec("cat /sys/class/net/eth0/address");
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            sValue = input.readLine();
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //return sValue.toLowerCase();
        return sValue;
    }
*/




    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}