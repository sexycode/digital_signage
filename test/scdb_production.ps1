param(
    [string]$scdb_ip,
    [string]$apk
)

if ([string]::IsNullOrEmpty($scdb_ip) -or [string]::IsNullOrEmpty($apk) )
{
	Write-Host "������ IP �ּ� �� ��ġ����apk �̸��� �Է��ϼ���"
}
else
{
	adb connect $scdb_ip
	$device = adb devices | select-string -pattern $scdb_ip
	Write-Host "[$device]"

	if ( $device -match "device")
	{
		Write-Host "----------adb root"
		adb -s $scdb_ip root
		Start-Sleep -Seconds 1
		Write-Host "----------remount "
		adb -s $scdb_ip shell mount -o rw,remount /system
		Start-Sleep -Seconds 2
		adb -s $scdb_ip uninstall com.sscctv.tempdisplayboard
		Start-Sleep -Seconds 1
		
		adb -s $scdb_ip shell mount 
		Write-Host "----------install"
		adb -s $scdb_ip push $apk /system/app/scdb.apk
		adb -s $scdb_ip push .\sscctv_install.sh /storage/emulated/0/
		adb -s $scdb_ip push .\sscctv_chmod.sh /storage/emulated/0/
		adb -s $scdb_ip push .\sscctv_hosts.sh /storage/emulated/0/
		Write-Host "----------reboot"
		adb -s $scdb_ip reboot
	}
	else
	{
		Write-Host "device not found"
	}
	
}

