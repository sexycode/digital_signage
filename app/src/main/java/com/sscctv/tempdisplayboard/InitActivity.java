package com.sscctv.tempdisplayboard;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sscctv.tempdisplayboard.databinding.ActivityInitBinding;
import com.sscctv.tempdisplayboard.util.KeyList;
import com.sscctv.tempdisplayboard.util.TinyDB;
import com.sscctv.tempdisplayboard.util.Utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Locale;
import java.util.Objects;

public class InitActivity extends AppCompatActivity {

    private ActivityInitBinding mBinding;
    private TinyDB tinyDB;
    private static final String TAG = InitActivity.class.getSimpleName();

    private static final String KEY_FIRST = "first";
    private static final String KEY_TIME_SET = "time_set";

    Utils utils = new Utils();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_init);
        tinyDB = new TinyDB(this);

        int flag = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_FULLSCREEN;

        flag |= View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
        flag |= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        flag |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        getWindow().getDecorView().setSystemUiVisibility(flag);

        @SuppressLint("HandlerLeak") Handler handler = new Handler() {
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                startActivity(new Intent(InitActivity.this, MainActivity.class));
                overridePendingTransition(R.anim.anim_left_in, R.anim.anim_right_out);
                finish();
            }
        };
        handler.sendEmptyMessageDelayed(0, 2000);

        String a = tinyDB.getString("config");
        if (a.length()>0)
            Log.d(TAG, "config : " + a);
        else {
            Log.d(TAG, "config : not found --> Loading default_config.json in assets");
            setDefaultConfig(tinyDB,getApplicationContext());
        }

        tinyDB.putString("ip", utils.getIPAddress(true));
//        Log.d(TAG, "first : " + tinyDB.getBoolean(KEY_FIRST));

    }

    public void setDefaultConfig(TinyDB tinyDB,Context context)
    {
        try {
            // create Gson instance
            Gson gson = new Gson();
            String reader = ReadFromfile("default_config.json", context);
            // convert JSON string to User object
            JsonObject default_config = gson.fromJson(reader, JsonObject.class).getAsJsonObject("set");
            // add ip , mac to json

            default_config.addProperty("ip",utils.getIPAddress(true));
            default_config.addProperty("mac",utils.getMacAddress("eth0"));
            default_config.addProperty("config","default");

            // print user object
            System.out.println(default_config);
            utils.jsonPutDB(tinyDB,default_config);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public String ReadFromfile(String fileName, Context context) {
        StringBuilder returnString = new StringBuilder();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;
        try {
            fIn = context.getResources().getAssets()
                    .open(fileName, Context.MODE_WORLD_READABLE);
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);
            String line = "";
            while ((line = input.readLine()) != null) {
                returnString.append(line);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (isr != null)
                    isr.close();
                if (fIn != null)
                    fIn.close();
                if (input != null)
                    input.close();
            } catch (Exception e2) {
                e2.getMessage();
            }
        }
        return returnString.toString();
    }


    @Override
    protected void onResume() {
        super.onResume();

//        initTimeZone();
//        initDevice();
    }

    private void initTimeZone() {
        final AlarmManager systemService = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Objects.requireNonNull(systemService).setTimeZone("GMT+9:00");
    }

    private void initDevice() {
        String phrase = tinyDB.getString(KeyList.KEY_STRING_PHRASE);
        if(phrase.equals("")) {
            tinyDB.putString(KeyList.KEY_STRING_PHRASE, "환영합니다. 문구를 설정해주세요.");
        }

        tinyDB.putInt(KeyList.KEY_STRING_COLOR, Color.WHITE);
        tinyDB.putInt(KeyList.KEY_STRING_SPEED, 1);
    }


}
