package com.sscctv.tempdisplayboard.util;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sscctv.tempdisplayboard.MainActivity;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class Utils extends AppCompatActivity  {

    private static final String TAG=Utils.class.getSimpleName();
    public static <T> boolean notEmpty(List<T> list) {
        return !isEmpty(list);
    }

    public static <T> boolean isEmpty(List<T> list) {
        if (list == null || list.size() == 0) {
            return true;
        }
        return false;
    }

    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    public static int dip2px(Context context, float dipValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }

    public static int px2sp(Context context, float pxValue) {
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (pxValue / fontScale + 0.5f);
    }

    public static int sp2px(Context context, float spValue) {
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (spValue * fontScale + 0.5f);
    }

    public static int getWindowWidth(Activity context) {
        DisplayMetrics metric = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(metric);
        return metric.widthPixels;
    }

    public static int getWindowHeight(Activity context) {
        DisplayMetrics metric = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(metric);
        return metric.heightPixels;
    }

    private static boolean isChinese(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS
                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION) {
            return true;
        }
        return false;
    }

    public static boolean isChinese(String strName) {
        char[] ch = strName.toCharArray();
        for (int i = 0; i < ch.length; i++) {
            char c = ch[i];
            if (isChinese(c)) {
                return true;
            }
        }
        return false;
    }
    /*
        public String LoadData(String inFile) {
            String tContents = "";

            try {
                InputStream stream = getAssets().open(inFile);

                int size = stream.available();
                byte[] buffer = new byte[size];
                stream.read(buffer);
                stream.close();
                tContents = new String(buffer);
            } catch (IOException e) {
                // Handle exceptions here
            }
            return tContents;
        }
     */
    public int jsonPutDB(TinyDB tinyDB,JsonObject jo) {
        String TAG = getClass().getSimpleName();
        Set<Map.Entry<String, JsonElement>> entries = jo.entrySet();
        for (Map.Entry<String ,JsonElement> entry: entries) {
//            Log.d(TAG, entry.getKey() + " ---> "+ entry.getValue());
            if (jsonCheck(jo,entry.getKey())==0) {
                tinyDB.putString(entry.getKey(), jo.get(entry.getKey()).getAsString());
            }
            else {
                Log.d(TAG, entry.getKey() + " key is not found or value is null");
                return -1;
            }
        }
        return 0;
    }

    public int jsonGetDB(TinyDB tinyDB, JsonObject jo) {
        String TAG = getClass().getSimpleName();

        Set<Map.Entry<String,JsonElement>> entries = jo.entrySet();
        for (Map.Entry<String ,JsonElement> entry: entries) {
            Log.d(TAG, entry.getKey() + " ---> "+ entry.getValue());
//            if (jsonCheck(jo,entry.getKey())==0) {
//                jo.remove(entry.getKey());
            if (!(tinyDB.getString(entry.getKey()).isEmpty())) {
                Log.d(TAG , "key : " + entry.getKey() + " value : " + tinyDB.getString((entry.getKey())));
                jo.addProperty(entry.getKey(),tinyDB.getString(entry.getKey()));
            }
//            }
//            else {
//                Log.d(TAG, entry.getKey() + " key is not found or value is null");
//                return -1;
//            }
        }
        return 0;
    }
    public int jsonCheck(JsonObject jo,String _key) {
        String TAG = getClass().getSimpleName();
        if (jo.get(_key) == null) {
//            Log.d(TAG, _key + " not found");
            return -3;
        } else if ((jo.get(_key).isJsonNull())) {
//            Log.d(TAG, _key+ "'s value is null");
            return -2;
        } else if (jo.get(_key).getAsString().length()==0) {
//            Log.d(TAG,  "value length is 0 --> ["+ _key + "]->[" + jo.get(_key).getAsString() + "]");
            return -1;
        } else {
//            Log.d(TAG,  "["+ _key + "]->[" + jo.get(_key).getAsString() + "]");
            return 0;
        }
    }

    /**
     * Get IP address from first non-localhost interface
     * @param useIPv4 true=return ipv4, false=return ipv6
     * @return  address or empty string
     */
    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress().toUpperCase();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':')<0;
                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 port suffix
                                return delim<0 ? sAddr : sAddr.substring(0, delim);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) { } // for now eat exceptions
        return "";
    }
    /**
     * Returns MAC address of the given interface name.
     * @param interfaceName eth0, wlan0 or NULL=use first interface
     * @return  mac address or empty string
     */
    public static String getMacAddress(String interfaceName) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (interfaceName != null) {
                    if (!intf.getName().equalsIgnoreCase(interfaceName)) continue;
                }
                byte[] mac = intf.getHardwareAddress();
                if (mac==null) return "";
                StringBuilder buf = new StringBuilder();
                for (int idx=0; idx<mac.length; idx++)
                    buf.append(String.format("%02X:", mac[idx]));
                if (buf.length()>0) buf.deleteCharAt(buf.length()-1);
                return buf.toString();
            }
        } catch (Exception ex) { } // for now eat exceptions
        return "";
        /*try {
            // this is so Linux hack
            return loadFileAsString("/sys/class/net/" +interfaceName + "/address").toUpperCase().trim();
        } catch (IOException ex) {
            return null;
        }*/
    }
    private String getModel() {
        return KeyList.MODEL_NAME;
    }

    private String getLocation(TinyDB tinyDB) {
        String loc="설정 안됨";
        /* location */
        if (!(tinyDB.getString("location").isEmpty())) {
            loc = tinyDB.getString("location");
        }
/*
        String loc = tinyDB.getString(KeyList.KEY_SETUP_LOCATION);
        if (loc.equals("")) {
            return "설정 안됨";
        }
*/
        return loc;
    }

    public static String getSerialNumber() {
        String serialNumber;

        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", String.class);
            serialNumber = (String) get.invoke(c, "ro.boot.serialno");
        } catch (Exception e) {
            e.printStackTrace();
            serialNumber = null;
        }

        return serialNumber;
    }

    public boolean getEthMode() {
        String eth0;

        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", String.class);
            eth0 = (String) get.invoke(c, "init.svc.dhcpcd_eth0");
        } catch (Exception e) {
            e.printStackTrace();
            eth0 = null;
        }

        return Objects.requireNonNull(eth0).equals("stopped");
    }

    public  boolean isStoragePermissionGranted(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(activity,Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(activity , new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            } else {
                Log.v(TAG,"Permission is granted");
                return true;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }

    public void RestartApp(Activity activity)
    {
        Intent mStartActivity = new Intent(activity, MainActivity.class);
        int mPendingIntentId = 123456;
        PendingIntent mPendingIntent = PendingIntent.getActivity(activity, mPendingIntentId,    mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager)activity.getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
        System.exit(0);
    }

    public static void InstallAPK(String filename){
        File file = new File(filename);
        if(file.exists()){
            try {
                String command;
                Process proc;

//                RootShell.suOutputExecute("id");
//                command = "mount -o rw,remount /system";
//
//                Log.d(TAG,"mount");
//                Process proc = Runtime.getRuntime().exec(new String[] { "su", "-c", command });
//                proc.waitFor();
//                RootShell.suOutputExecute("mount");
//
                Log.d(TAG,"chmod");
                command = "chmod 777 " + filename;
//                RootShell.suOutputExecute(command);
                proc = Runtime.getRuntime().exec(new String[] { "su", "-c", command });
                proc.waitFor();
//
//                Log.d(TAG,"ls");
//                command = "ls -al " + filename;
//                RootShell.suOutputExecute(command);
//
//                Log.d(TAG,"mv");
//                command = "mv " + filename + " /system/app/scdb.apk";
//                RootShell.suOutputExecute(command);

                command = "sh /storage/emulated/0/sscctv_install.sh " + filename;
                Log.d(TAG,"execute sscctv.sh "+ filename);
                RootShell.suOutputExecute(command);
//                proc = Runtime.getRuntime().exec(new String[] { "su", "-c", command });
//                proc.waitFor();


                command = "reboot";
                Log.d(TAG,"Reboot by SCDB");
                proc = Runtime.getRuntime().exec(new String[] { "su", "-c", command });
//                Runtime.getRuntime().exec(new String[] { "reboot" });


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void setNTP(String ntp_server){
            try {
//                Process proc;

//                RootShell.suOutputExecute("id");
//                command = "mount -o rw,remount /system";
//
//                Log.d(TAG,"mount");
//                Process proc = Runtime.getRuntime().exec(new String[] { "su", "-c", command });
//                proc.waitFor();
//                RootShell.suOutputExecute("mount");
//

                String command = "settings put global ntp_server " + ntp_server;
                Log.d("setNTP start" , command);
                RootShell.suOutputExecute(command);
                Log.d("setNTP end" , command);
//                proc = Runtime.getRuntime().exec(new String[] { "su", "-c", command });
//                proc.waitFor();


//                command = "reboot";
//                Log.d(TAG,"Reboot by SCDB");
//                proc = Runtime.getRuntime().exec(new String[] { "su", "-c", command });

            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    public void setNTP1(String ntp_server) throws InterruptedException, IOException {
//        setNTP(ntp_server);
        String command="sh /storage/emulated/0/sscctv_hosts.sh " + ntp_server;
        Process proc;
        Log.d("setNTP1 start" , command);
        proc = Runtime.getRuntime().exec(new String[] { "su", "-c", command });
        proc.waitFor();
        Log.d("setNTP1 end" , command);
    }

    public void sscctvReboot() throws InterruptedException, IOException {
        String command="reboot";
        Process proc;
        Log.d(TAG,"Reboot Start");
        proc = Runtime.getRuntime().exec(new String[] { "su", "-c", command });
        proc.waitFor();
    }

    public void chmodDirectory() throws InterruptedException, IOException {
        String command="sh /storage/emulated/0/sscctv_chmod.sh";
        Process proc;
        proc = Runtime.getRuntime().exec(new String[] { "su", "-c", command });
        proc.waitFor();
    }

    public boolean saveNetConfig(String ipAddr, String netMask, String netGate,String DNS) throws IOException, InterruptedException {
        chmodDirectory();
        try {
            final String ipConfigFile = Environment.getDataDirectory() + "/misc/ethernet/ipconfig.txt";
            File file = new File(ipConfigFile);
            FileOutputStream fos = new FileOutputStream(file);
            DataOutputStream out = new DataOutputStream(fos);

            final int IPCONFIG_FILE_VERSION = 2;
            out.writeInt(IPCONFIG_FILE_VERSION);

            final String IP_ASSIGNMENT_KEY = "ipAssignment";
            out.writeUTF(IP_ASSIGNMENT_KEY);
            out.writeUTF("STATIC");

            final String LINK_ADDRESS_KEY = "linkAddress";
            out.writeUTF(LINK_ADDRESS_KEY);
            out.writeUTF(ipAddr);
            out.writeInt(24); // The address prefix 192.168.1.110 is the prefix 192.168.1 is 24

            final String GATEWAY_KEY = "gateway";
            out.writeUTF(GATEWAY_KEY);
            out.writeInt(0);  // Default route.
            out.writeInt(1);  // Have a gateway.
            out.writeUTF(netGate);

            final String DNS_KEY = "dns";
            out.writeUTF(DNS_KEY);
//            out.writeUTF(netGate); // General router, gateway is also its DNS
//            out.writeUTF(DNS_KEY);
            out.writeUTF(DNS);

            final String PROXY_SETTINGS_KEY = "proxySettings";
            out.writeUTF(PROXY_SETTINGS_KEY);
            out.writeUTF("NONE");

            final String ID_KEY = "id";
            out.writeUTF(ID_KEY);
            out.writeInt(0);

            final String EOS = "eos";
            out.writeUTF(EOS);

            out.flush();
            out.close();
            Runtime.getRuntime().exec(new String[] { "su", "-c", "reboot"});

        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public static void setNtpServer(String ntp_server) throws IOException {
        String cmd = "settings put global ntp_server " + ntp_server;
        Log.d(TAG,cmd);
        Runtime.getRuntime().exec(new String[] { "su", "-c", cmd});
        //RootShell.suExecute(new String[] { cmd });

        Log.d(TAG,"Reboot after ntp setting");
        cmd = "reboot";
//        Runtime.getRuntime().exec(new String[] { "su", "-c", "reboot"});
        Runtime.getRuntime().exec(cmd);
/*
        Process mProcess = new ProcessBuilder().command("/system/xbin/su").redirectErrorStream(true).start();
        OutputStream out = mProcess.getOutputStream();
        Log.d(TAG, "Native command = " + cmd);
        out.write(cmd.getBytes());
 */
    }



    /*
    public boolean configureNative(Activity activity,String interfaceName, int ipAddress, int netmask, int gateway, int dns1, int dns2)
    {
        ConnectivityManager mService = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        EthernetManager mEthManager = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mEthManager = (EthernetManager) activity.getSystemService(Context.ETHERNET_SERVICE);
        }
        if (mEthManager.getState() == EthernetManager.ETHERNET_STATE_ENABLED) {
            EthernetDevInfo mInterfaceInfo = mEthManager.getSavedConfig();
            String mIp;
            String mMask;
            String mGw;
            String mDns;

            mIp = "192.168.0.118";
            mMask = "255.255.255.0";
            mGw = "192.168.0.1";
            mDns = "192.168.0.1";
            mInterfaceInfo.setConnectMode(EthernetDevInfo.ETHERNET_CONN_MODE_MANUAL);
            mInterfaceInfo.setIpAddress(mIp);
            mInterfaceInfo.setNetMask(mMask);
            mInterfaceInfo.setDnsAddr(mDns);
            mInterfaceInfo.setGateWay(mGw);
            try {
                mEthManager.updateDevInfo(mInterfaceInfo);
                Thread.sleep(500);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Log.d(TAG,"ethernet disabled");
//            Toast.makeText(this, "Ethernet state disabled!", 5000).show();
        }
    }
     */
}