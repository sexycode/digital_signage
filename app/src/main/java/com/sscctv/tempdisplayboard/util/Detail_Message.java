package com.sscctv.tempdisplayboard.util;

import android.util.Log;

public class Detail_Message {
        String type;
        String message;
        public Detail_Message(String type,String message) {
            this.type=type;
            this.message=message;
        }

        public String getType() {
            return this.type;
        }
        public String getMessage() {
            return this.message;
        }
}
